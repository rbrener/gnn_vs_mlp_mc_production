#include <iostream>
#include <sstream>
#include <math.h>
#include "Pythia8/Pythia.h"
/*
#include "TH1.h"

// ROOT, for interactive graphics.
#include "TVirtualPad.h"
//#include "TApplication.h"

// ROOT, for saving file.
#include "TFile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TROOT.h"
*/
//#include "Zprimettbar_generatorDct.cc"
//#include "Pythia8Plugins/HepMC2.h"


using namespace Pythia8;

//TFile* file;
//TTree* tree;
//TTree* tree_qq2qq;
//TTree* tree_qqbar2qqbar;
//TTree* tree_qq2qq_and_qqbar2qqbar;

/*
vector<TLorentzVector>* p4 = nullptr;
vector<int>* 		id = nullptr;
vector<vector<int> >* 	parents = nullptr;
vector<vector<int> >*	children = nullptr;
vector<float>*        y_tuple = nullptr;
vector<float>*        pT_tuple = nullptr;
float			Q;
float			Q2;
float			aS;
float			sHat;
float			tHat;
float			uHat;
float			mHat;
float           pTHat;
float           pTj1;
float           pTj2;
float           xs;
float           chi;
float           y_star;
float 		E, pz;
*/
//vector<int>*		status = nullptr;

//Everything inside main for now
//============================================================================
int main(int argc, char *argv[]) {

char *random_seed = argv[1];
char *nEvents_char = argv[2];
char *mZp_char     = argv[3];
std::string full_path = "/storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig/";

char process[] = "Zprimettbar_lephad";

std::string str_random_seed(random_seed);
std::string str_process(process);
std::string str_mZp(mZp_char);
string filename = str_process+"_mZp"+str_mZp+"_randomSeed"+str_random_seed ;

std::cout << "Filename:   " << filename << std::endl;


// Pythia stuff

    Pythia pythia;

    pythia.readString("NewGaugeBoson:ffbar2gmZZprime = on");
    pythia.readString("Zprime:gmZmode = 3");
    pythia.readString("Zprime:universality = off");
    pythia.readString("32:m0 = "+str_mZp);
    pythia.readString("32:onMode = off");
    pythia.readString("32:onIfAny = 6");
    pythia.readString("24:onMode = off");
    pythia.readString("24:onPosIfAny = 11 13");
    pythia.readString("24:onNegIfAny = 1 2 3 4 5");

    pythia.readString("Beams:eCM = 13000.");

    //Create and open file for LHEF 3.0
    //pythia.readString("Beams:frameType = 4");
    LHEF3FromPythia8 myLHEF3(&pythia.event, &pythia.info);
    myLHEF3.openLHEF(full_path+filename+".lhe");
    myLHEF3.setInit();
    pythia.readString("Random:setSeed = on");
    pythia.readString("Random:seed = "+str_random_seed);
    pythia.init();

int nEvents = std::stoi(nEvents_char);
// Begin event loop. Generate event; skip if generation aborted.
  for (int iEvent = 0; iEvent < nEvents; ++iEvent) {
    if (!pythia.next()) continue;
	myLHEF3.setEvent();
    }
 	pythia.stat();
	myLHEF3.closeLHEF(true);

 	std::cout << "The xs is:   " << pythia.info.sigmaGen() << std::endl;


  return 0;
}
