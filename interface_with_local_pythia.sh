#!bin/bash

#switch '/home/rbrener/Programs' to your own Pythia8 clone path
LOCAL_PYTHIA_DIR="/home/rbrener/Programs/pythia8303/examples"

for file in ./mc_generation/*; do
  file_basename=$(basename ${file})
  full_path=$(readlink -f ${file})
  if [[ -f "${LOCAL_PYTHIA_DIR}/${file_basename}" ]];
  then
      DIFF=$(diff ${LOCAL_PYTHIA_DIR}/${file_basename} ${full_path} )
      if [ "$DIFF" != "" ]
      then
          echo "Resolve difference for file ${file_basename} manually"
      else
          rm ${LOCAL_PYTHIA_DIR}/${file_basename}
          ln -s ${full_path} ${LOCAL_PYTHIA_DIR}/.
      fi
  elif  [[ -h "${LOCAL_PYTHIA_DIR}/${file_basename}" ]];
  then
          echo "Already exists as symlink; no need for further action"
  else
          ln -s ${full_path} ${LOCAL_PYTHIA_DIR}/.
  fi
done
