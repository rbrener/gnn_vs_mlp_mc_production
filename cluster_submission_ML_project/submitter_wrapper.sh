#!/bin/bash
COUNTER=0

PathSig="/storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig"
PathBkg="/storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Bkg"


declare -a RandomSeeds=($(seq 1 1 100))
#declare -a RandomSeeds=($(seq 1 1 500))

#declare -a RandomSeeds=($(seq 5001 1 6000))
#declare -a RandomSeeds=(1201 943 1931 881 1544 659 651 1840 1679 672 225 1715 1206 1329 970 1838 226 1034 1867 675 572 1728 1387 396 193 721 650 917 1363 394 214 596 1796 898 1347 614 1641 1174 928 663 772 8 1529 199 1258 1261 706 277 1793 263 974 837 1557 576 1477 185 9 235 198 639 1977 653 186 866 767 1953 934 583 661 1948 920 588 1681 998 202 945 1259 1431 768 1356 894 1361 187 1538 1795 200 1211 597 1601 712 610 1028 904 584 1213 1701 1543 1180 1298 1904 785 1430 1493 234 1355 1013 1346 557 1348 626 1302 249 1330 1197 1429 1629 847 1047 273 635 1119 569 1352 22 1570 1822 605 1623 1873 252 579 691 1336 343 1124 1928 1177 600 1428 1358 1686 1235 197 1350 191 163 1575 861 1781 873 601 669 560 267 757 1054 1153 718 1673 876 1362 1499 1789 913 1104 1136 644 587 991 612 265 1829 1583 725 581 201 1865 751 1905 758 180 1975 972 1893 1522 676 1203 1908 20 976 7 442 1162 556 1037 1885 1167 1981 760 1516 453 693 1709 708 688 1169 575 846 1052 613 775 224 1916 393 590 1080 754 1638 978 736 649 444 504 1999 864 274 1432 753 1710 1674 731 162 1929 702 1800 254 1787 658 216 1089 562 1247 977 652 593 1118 242 683 1001 759 1188 1290 684 992 6 1675 23 720 1611 971 1945 1040 580 21 1205 783 446 602 1660 1171 458 1792 947 710 1417 735 616 250 811 1775 1649 636 678 1586 711 232 656 247 209 729 1460 19 1525 957 391 797 737 697 1637 842 623 221 679 451 1644 732 1086 770 573 595 665 645 607 206 704 673 1676 276 1248 195 629 567 450 836 1166 454 1732 397 1517 1337 586 1281 261 1187 1422 1482 184 447 1249 744 177 1144)


declare -a mZps=(1000 1250 1500)
#declare -a mZps=(1500)

nEvents=10000
nEvents_signal=$((nEvents / 1))

for random_seed in ${RandomSeeds[@]}; do
  mkdir -p "${PathSig}/Log"
  mkdir -p "${PathSig}/Err"
  mkdir -p "${PathSig}/Out"

  echo "============================================================================"
  echo "Submitting Pythia8 Zprimettbar generation job w/ random_seed=${random_seed}"
  echo "============================================================================"
  for mZp in ${mZps[@]}; do
      qsub -q N -l io=3.5 -v seed=${random_seed},nEvents=${nEvents_signal},mZp=${mZp} -o "${PathSig}/Out" -e "${PathSig}/Err" -N "Pythia_Generating_Job_Zprimettbar_RandomSeed${random_seed}" /storage/agrp/roybr/Pythia8/cluster_submission_ML_project/pbs_executable_sigs.sh
  done
  #echo "============================================================================"
  #echo "Submitting Pythia8 ttbar generation job w/ random_seed=${random_seed}"
  #echo "============================================================================"
  #mkdir -p "${PathBkg}/Log"
  #mkdir -p "${PathBkg}/Err"
  #mkdir -p "${PathBkg}/Out"
  #qsub -q N -l io=3.5 -v seed=${random_seed},nEvents=${nEvents} -o "${PathBkg}/Out" -e "${PathBkg}/Err" -N "Pythia_Generating_Job_ttbar_RandomSeed${random_seed}" /storage/agrp/roybr/Pythia8/cluster_submission_ML_project/pbs_executable_bkgs.sh
  let "COUNTER+=1"
#  fi
  if [ $COUNTER -eq 5000 ]; then
  break
  fi
done
