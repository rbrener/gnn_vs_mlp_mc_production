#!/bin/bash

#PBS -m n
#PBS -l walltime=12:00:00

#source /srv01/agrp/roybr/.bashrc
#source /srv01/agrp/roybr/setup_atlas.sh


random_seed=$seed
nEvents=$nEvents

echo "##################################"
echo "This is random_seed   ${random_seed}"
echo "##################################"

#cd /storage/agrp/roybr/Pythia8/pythia8312/examples
cd /storage/agrp/roybr/Pythia8/pythia8312_Alma9_new/pythia8312/examples

#./main_ttbar_generator_leplep ${random_seed} ${nEvents}
./main_ttbar_generator_lephad ${random_seed} ${nEvents}
./main_ttbar_generator_hadlep ${random_seed} ${nEvents}
#./main_ttbar_generator_hadhad ${random_seed} ${nEvents}

echo "Finished generating Pythia sample; about to convert w/ Delphes"

#cd /srv01/agrp/roybr/delphes
#cd /srv01/agrp/roybr/delphes_Alma9
cd /srv01/agrp/roybr/delphes_Alma9/Delphes-3.5.0

source /srv01/agrp/roybr/delphes_Alma9/Delphes-3.5.0/setup.sh
#source /srv01/agrp/roybr/delphes_Alma9/Delphes-3.5.0/DelphesEnv.sh

#source /srv01/agrp/roybr/zprimeplusxntr/setup.sh
#source /srv01/agrp/roybr/delphes/DelphesEnv.sh


#echo "Smearing ttbar->leplep w/ Delphes"
#echo "========================================="
#./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Bkg/ttbar_leplep_Delphes_Smeared_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Bkg/ttbar_leplep_randomSeed${random_seed}.lhe

echo "Smearing ttbar->lephad w/ Delphes"
echo "========================================="
./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Bkg/ttbar_lephad_Delphes_Smeared_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Bkg/ttbar_lephad_randomSeed${random_seed}.lhe

echo "Smearing ttbar->hadlep w/ Delphes"
echo "========================================="
./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Bkg/ttbar_hadlep_Delphes_Smeared_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Bkg/ttbar_hadlep_randomSeed${random_seed}.lhe


#echo "Smearing ttbar->hadhad w/ Delphes"
#echo "========================================="
#./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Bkg/ttbar_hadhad_Delphes_Smeared_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Bkg/ttbar_hadhad_randomSeed${random_seed}.lhe


echo "Finished generating smeared sample w/ Delphes; check output"
