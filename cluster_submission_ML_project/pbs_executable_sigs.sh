#!/bin/bash

#PBS -m n
#PBS -l walltime=12:00:00

#source /srv01/agrp/roybr/.bashrc
#source /srv01/agrp/roybr/setup_atlas.sh


random_seed=$seed
nEvents=$nEvents
mZp=$mZp

echo "##################################"
echo "This is random_seed   ${random_seed}"
echo "##################################"

#cd /storage/agrp/roybr/Pythia8/pythia8312/examples
cd /storage/agrp/roybr/Pythia8/pythia8312_Alma9_new/pythia8312/examples

#./main_Zprimettbar_generator_leplep ${random_seed} ${nEvents} ${mZp}
#./main_Zprimettbar_generator_lephad ${random_seed} ${nEvents} ${mZp}
./main_Zprimettbar_generator_lephad_HEPMC3 ${random_seed} ${nEvents} ${mZp}
#./main_Zprimettbar_generator_hadlep ${random_seed} ${nEvents} ${mZp}
./main_Zprimettbar_generator_hadlep_HEPMC3 ${random_seed} ${nEvents} ${mZp}
#./main_Zprimettbar_generator_hadhad ${random_seed} ${nEvents} ${mZp}

echo "Finished generating Pythia sample; about to convert w/ Delphes"

#cd /srv01/agrp/roybr/delphes
#cd /srv01/agrp/roybr/delphes_Alma9
cd /srv01/agrp/roybr/delphes_Alma9/Delphes-3.5.0

source /srv01/agrp/roybr/delphes_Alma9/Delphes-3.5.0/setup.sh
#source /srv01/agrp/roybr/delphes_Alma9/Delphes-3.5.0/DelphesEnv.sh

#source /srv01/agrp/roybr/zprimeplusxntr/setup.sh
#source /srv01/agrp/roybr/delphes/DelphesEnv.sh


#echo "Smearing Zprime->ttbar->leplep w/ Delphes"
#echo "========================================="
#./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Sig/Zprimettbar_leplep_Delphes_Smeared_mZp${mZp}_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig/Zprimettbar_leplep_mZp${mZp}_randomSeed${random_seed}.lhe

echo "Smearing Zprime->ttbar->lephad w/ Delphes"
echo "========================================="
#./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Sig/Zprimettbar_lephad_Delphes_Smeared_mZp${mZp}_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig/Zprimettbar_lephad_mZp${mZp}_randomSeed${random_seed}.lhe
./DelphesHEPMC3 cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Sig/Zprimettbar_lephad_Delphes_Smeared_mZp${mZp}_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig/Zprimettbar_lephad_mZp${mZp}_randomSeed${random_seed}.hepmc

echo "Smearing Zprime->ttbar->hadlep w/ Delphes"
echo "========================================="
#./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Sig/Zprimettbar_hadlep_Delphes_Smeared_mZp${mZp}_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig/Zprimettbar_hadlep_mZp${mZp}_randomSeed${random_seed}.lhe
./DelphesHEPMC3 cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Sig/Zprimettbar_hadlep_Delphes_Smeared_mZp${mZp}_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig/Zprimettbar_hadlep_mZp${mZp}_randomSeed${random_seed}.hepmc

#echo "Smearing Zprime->ttbar->hadhad w/ Delphes"
#echo "========================================="
#./DelphesLHEF cards/delphes_card_ATLAS.tcl /storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar/Sig/Zprimettbar_hadhad_Delphes_Smeared_mZp${mZp}_RandomSeed${random_seed}.root /storage/agrp/roybr/Pythia8/Pythia8_Outputs/Zprimettbar/Sig/Zprimettbar_hadhad_mZp${mZp}_randomSeed${random_seed}.lhe


echo "Finished generating smeared sample w/ Delphes; check output"
