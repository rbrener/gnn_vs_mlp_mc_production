#!/usr/bin/env bash
(
  [[ -n $BASH_VERSION ]] && (return 0 2>/dev/null)
) && sourced=1 || sourced=0

if [[ "${sourced}" == 0 ]]; then
    echo "Please do not `execute` this file, but `source` it."
    echo "See, e.g., https://superuser.com/questions/176783/."
    exit 1
fi


echo "Adding the current directory to the PYTHONPATH..."
dir="$( dirname $( readlink -f "${BASH_SOURCE[0]}" ) )"
! echo "$PYTHONPATH" | egrep "(^|:)${dir%/}/?(:|$)" &>/dev/null && export PYTHONPATH="${dir}:${PYTHONPATH}"

if [[ "$2" == "athena" ]]; then
    echo "Using AnalysisBase..."
    export useAthena="1"
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
    asetup AnalysisBase,latest,master
fi

if [[ "$1" == "virtual" ]]; then
    echo "Creating a virtual environment for python..."
    export virtual="1"
    python3 -m venv .virtualenv
    source .virtualenv/bin/activate
else
    echo "Proceeding in your default environment for python..."
fi


echo "Installing the necessary python packages..."
python3 -m pip install pip --upgrade
if [[ ${useAthena}=="1" ]]; then
    echo "Relying on Athena. Installing the minimal number of additional python packages..." 
    python3 -m pip install -r requirements/requirementsMinimal.txt
else
    echo "Not relying on any shared framework. Installing all necessary additional python packages..."
    python3 -m pip install -r requirements/requirements.txt
fi

mkdir -p src/libs/configs
mkdir -p run/libs/configs
mkdir -p src/libs/graphs
mkdir -p run/libs/graphs
 
echo "Creating symbolic links for the necessary files..."
source .setup/symlinker.sh src/libs/configs run/libs/configs
source .setup/symlinker.sh run/libs/configs src/libs/configs
source .setup/symlinker.sh src/libs/graphs run/libs/graphs
source .setup/symlinker.sh run/libs/graphs src/libs/graphs

echo "Adding the .setup/ directory to the PATH..."
# make these scripts executable
chmod +x .setup/*.sh
! echo "$PATH" | egrep "(^|:)${dir%/}/.setup/?(:|$)" &>/dev/null && export PATH="${dir}/.setup:${PATH}"
