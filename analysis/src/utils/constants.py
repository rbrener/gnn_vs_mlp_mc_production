from particle import Particle
from hepunits import MeV, GeV, ps, nanometer, m, fm

### Masses of Particles ###
### ------------------- ###
_fMass = lambda x: Particle.from_name(x).mass
_particleDict = {"muon": "mu+", "electron": "e-", "W": "W+", "top": "t" }
masses = {}
for key, value in _particleDict.items():
    masses[key] = _fMass(value) / GeV
