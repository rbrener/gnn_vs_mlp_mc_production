import logging
import coloredlogs


class CustomAdapter(logging.LoggerAdapter):

    def process(self, msg, kwargs):
        return "[%s] %s" % (self.extra["logger_name"], msg), kwargs


def get_logger(logger_name=__name__):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    format_str = " %(asctime)s - %(levelname)s - %(message)s"
    formatter = logging.Formatter(format_str)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    coloredlogs.install(
        level="DEBUG",
        logger=logger,
        fmt=format_str,
        level_styles={
            "debug": {
                "color": "green"
            },
            "info": {
                "color": "blue"
            },
            "warning": {
                "color": "yellow"
            },
            "error": {
                "color": "red"
            },
            "critical": {
                "color": "red",
                "bold": True
            },
        },
    )
    custom_logger = CustomAdapter(logger, {"logger_name": logger_name})
    return custom_logger
