import os
import copy
from src.utils import ioutils
from src.utils import logger as logging
import src
import glob

logger = logging.get_logger(__name__)


def parse_list_as_python(
        config,
        eval_trigger=lambda x: x.startswith("eval:"),
        eval_func=lambda x: eval(x[5:]),
):
    for i, val in enumerate(config):
        if isinstance(val, dict):
            config[i] = parse_dict_as_python(val)
        elif isinstance(val, list):
            config[i] = parse_list_as_python(val)
        elif isinstance(val, str) and eval_trigger(val):
            val = eval_func(val)
            config[i] = val
    return config


def parse_dict_as_python(
        config,
        eval_trigger=lambda x: x.startswith("eval:"),
        eval_func=lambda x: eval(x[5:]),
):
    for key, val in config.items():
        if isinstance(val, dict):
            config[key] = parse_dict_as_python(val)
        elif isinstance(val, list):
            config[key] = parse_list_as_python(val)
        elif isinstance(val, str) and eval_trigger(val):
            val = eval_func(val)
            config[key] = val
    return config


def flatten_dict(d, parent_key="", sep="."):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def unflatten_dict(d, sep="."):
    result_dict = {}
    for key, value in d.items():
        parts = key.split(sep)
        d = result_dict
        for part in parts[:-1]:
            if part not in d:
                d[part] = {}
            d = d[part]
        d[parts[-1]] = value
    return result_dict


def collapse_keys(d, sep=".", to_collapse="inherit"):
    new_d = copy.deepcopy(d)
    flat_dict = flatten_dict(new_d, sep=sep)
    new_d = {}
    for key, val in flat_dict.items():
        new_d[key.replace(sep + to_collapse, "")] = val
    flat_dict = copy.deepcopy(new_d)
    new_d = {}
    for key, val in flat_dict.items():
        new_d[key.replace(to_collapse + sep, "")] = val
    return new_d


def inherit_config(config, config_dir, will="inherit"):
    sep = "."
    flat_config = flatten_dict(config, sep=sep)
    for key, val in flat_config.items():
        if isinstance(val, str):
            if key.endswith(will):
                val = ioutils.get_path(val, config_dir)
                flat_config[key] = load_config(val, parse_python=False)
    flat_config = collapse_keys(flat_config, sep=sep, to_collapse=will)
    return unflatten_dict(flat_config, sep=sep)


def load_config(path, parse_python=True):
    if isinstance(path, dict):
        return path
    config_dir = os.path.dirname(path)
    config = ioutils.load_dict(ioutils.get_path(path, config_dir))
    config = inherit_config(config, config_dir)
    if parse_python:
        config = parse_dict_as_python(config)
    return config


libsDir = os.path.join(src.__path__[0], "libs")


def getPath(kernel, maxDepth=1):
    paths = []
    for i in range(maxDepth):
        paths.extend(glob.glob(os.path.join(libsDir, f"{kernel}")))
        kernel = f"*/{kernel}"
    if len(paths) == 0:
        logger.error(
            f"Could not find {kernel} in {libsDir} up to depth {maxDepth}!")
        return None
    if len(paths) > 1:
        logger.warning(
            f"Found multiple instances of {kernel} in {libsDir} up to depth {maxDepth}!"
        )
        logger.warning(f"Returning the first instance: {paths[0]}")
        logger.debug(f"All found instances: {paths}")
    logger.debug(f"Returning {paths[0]}")
    return paths[0]
