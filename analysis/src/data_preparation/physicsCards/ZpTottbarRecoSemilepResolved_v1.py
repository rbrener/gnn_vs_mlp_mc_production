### Imports ###
### ------- ###

### Imports -> Matrix/Array Manipulation ###
### ------------------------------------ ###
import numpy as np
import pandas as pd

### Imports -> Math ###
### --------------- ###
import itertools

### Imports -> Scientific Utilities ###
### ------------------------------- ###
import vector

### Imports -> Python Utilities ###
### --------------------------- ###
import copy

### Imports -> I/O ###
### -------------- ###
from tqdm import tqdm

### Imports -> Internals ###
### -------------------- ###
from src.utils import constants
from src.utils import logger as logging
from src.data_preparation import utils

### Logging Configuration ###
### --------------------- ###
default_logger = logging.get_logger( __name__ )
randomDebugger = utils.randomDebugger( default_logger )

### Physics Card ###
### ------------ ###

def addMinimumDeltaR( refContainer, container, containerSelections = {}, minDeltaRName = "minDeltaR" ):
    for refObject in refContainer:
        minDeltaR = np.inf 
        refObjectVec = vector.MomentumObject3D.from_rhophieta(
            refObject[ "pt"  ],
            refObject[ "phi" ],
            refObject[ "eta" ]
        )
        for containerObject in container:
            containerObjectVec = vector.MomentumObject3D.from_rhophieta(
                containerObject[ "pt"  ],
                containerObject[ "phi" ],
                containerObject[ "eta" ]
            )
            if "ptThreshold" in containerSelections.keys():
                if containerObject[ "pt" ] < containerSelections[ "ptThreshold" ]: continue
            deltaR = containerObjectVec.deltaR( refObjectVec )
            if deltaR < minDeltaR:
                minDeltaR = deltaR 
        refObject[ minDeltaRName ] = minDeltaR
    return refContainer


def addIsolation( 
        leptons, 
        tracks, 
        coneSize = 0.2, 
        isoName = "trackIsoCone20",
        customConeSizeFunc = None,
        relativeIsoName = None
    ):
    tracks_vecs = []
    for track in tracks:
        track_vec = vector.MomentumObject3D.from_rhophieta(
            track[ "pt"   ],
            track[ "phi"  ],
            track[ "eta"  ],
        )
        tracks_vecs.append( track_vec )
    for lepton in leptons:
        lepton_vec = vector.MomentumObject3D.from_rhophieta(
            lepton[ "pt"   ],
            lepton[ "phi"  ],
            lepton[ "eta"  ],
        )
        isolation = 0
        for track_vec in tracks_vecs:
            if customConeSizeFunc is not None:
                coneSize = customConeSizeFunc( lepton )
            if track_vec.deltaR( lepton_vec ) < coneSize:
                isolation += track_vec.rho
        lepton[ isoName ] = isolation
        if relativeIsoName is not None:
            lepton[ relativeIsoName ] = isolation / lepton[ "pt" ]
    return leptons

def addOverlapFlags( 
        jets,
        leptons,  
        strategy = "deltaR_lep_jet_sequential",
        logger   = default_logger, 
        **kwargs 
    ):
    randomDebugger.addFunc( addOverlapFlags, 0.01 )
    if strategy == "deltaR_lep_jet_sequential":
        argsNeeded = [ 
            "firstDeltaRThreshold", 
            "secondDeltaRThreshold",
            "leptonPtThreshold",
            "jetPtThreshold",
            "overflapFlagName"
        ]
        deltaRFunc = lambda vec1, vec2: vec1.deltaR( vec2 )
        if kwargs.get( "customDeltaRFunc", None ) is not None:
            deltaRFunc = kwargs[ "customDeltaRFunc" ]
        for neededArg in argsNeeded:
            if neededArg not in kwargs.keys():
                logger.error( f"Please provide the `{neededArg}` parameter for the `deltaR_sequential` strategy of overlap removal." )
                return leptons, jets
        closestJetIdxForLeptons = []
        closestJetDeltaRForLeptons = []
        closestLeptonIdxForJets = []
        closestLeptonDeltaRForJets = []
        leptonVecs = [ vector.MomentumObject3D.from_rhophieta( lepton[ "pt" ], lepton[ "phi" ], lepton[ "eta" ] ) for lepton in leptons ]
        jetVecs    = [ vector.MomentumObject3D.from_rhophieta( jet[ "pt" ], jet[ "phi" ], jet[ "eta" ] ) for jet in jets ]
        leptonsAlive = np.array( [ lep[ "pt" ] > kwargs[ "leptonPtThreshold" ] for lep in leptons ] )
        jetsAlive    = np.array( [ jet[ "pt" ] > kwargs[ "jetPtThreshold" ] for jet in jets ] )
        for leptonIdx, leptonVec in enumerate( leptonVecs ):
            if not leptonsAlive[ leptonIdx ]:
                closestJetIdxForLeptons.append( np.inf )
                closestJetDeltaRForLeptons.append( np.inf )
                continue
            closestJetIdx = np.inf
            closestJetDeltaR = np.inf
            for jetIdx, jetVec in enumerate( jetVecs ):
                if not jetsAlive[ jetIdx ]:
                    continue
                deltaR = deltaRFunc( leptonVec, jetVec )
                if deltaR < closestJetDeltaR:
                    closestJetDeltaR = deltaR
                    closestJetIdx = jetIdx
            closestJetIdxForLeptons.append( closestJetIdx )
            closestJetDeltaRForLeptons.append( closestJetDeltaR )
        closestJetIdxForLeptons = np.array( closestJetIdxForLeptons )
        closestJetDeltaRForLeptons = np.array( closestJetDeltaRForLeptons )
        jetIdxMarkedForRemoval = closestJetIdxForLeptons[ np.array( closestJetDeltaRForLeptons < kwargs[ "firstDeltaRThreshold" ] ) ]
        debugMsg = "`closestJetIdxForLeptons` and `closestJetDeltaRForLeptons` are: "
        debugMsg += f"{closestJetIdxForLeptons}, {closestJetDeltaRForLeptons}"
        debugMsg += f"\n`jetIdxMarkedForRemoval` is: {jetIdxMarkedForRemoval}"
        randomDebugger.run( debugMsg )
        if np.any( np.isinf( jetIdxMarkedForRemoval ) ):
            logger.fatal( f"Found an infinite value in the `jetIdxMarkedForRemoval` array. This should not happen. Please check the logic of the overlap removal." )
            raise ValueError
        if np.any( np.isnan( jetIdxMarkedForRemoval ) ):
            logger.fatal( f"Found a NaN value in the `jetIdxMarkedForRemoval` array. This should not happen. Please check the logic of the overlap removal." )
            raise ValueError
        if len( jetIdxMarkedForRemoval ) > 0:
            jetsAlive[ jetIdxMarkedForRemoval ] = False

        for jetIdx, jetVec in enumerate( jetVecs ):
            if not jetsAlive[ jetIdx ]:
                closestLeptonIdxForJets.append( np.inf )
                closestLeptonDeltaRForJets.append( np.inf )
                continue
            closestLeptonIdx = np.inf
            closestLeptonDeltaR = np.inf
            for leptonIdx, leptonVec in enumerate( leptonVecs ):
                if not leptonsAlive[ leptonIdx ]:
                    continue
                deltaR = deltaRFunc( jetVec, leptonVec )
                if deltaR < closestLeptonDeltaR:
                    closestLeptonDeltaR = deltaR
                    closestLeptonIdx = leptonIdx
            closestLeptonIdxForJets.append( closestLeptonIdx )
            closestLeptonDeltaRForJets.append( closestLeptonDeltaR )
        closestLeptonIdxForJets = np.array( closestLeptonIdxForJets )
        closestLeptonDeltaRForJets = np.array( closestLeptonDeltaRForJets )
        leptonIdxMarkedForRemoval = closestLeptonIdxForJets[ np.array( closestLeptonDeltaRForJets < kwargs[ "secondDeltaRThreshold" ] ) ]
        if len( leptonIdxMarkedForRemoval ) > 0:
            leptonsAlive[ leptonIdxMarkedForRemoval ] = False
        for lepIdx, lep in enumerate( leptons ):
            lep[ kwargs[ "overflapFlagName" ] ] = not leptonsAlive[ lepIdx ]
        for jetIdx, jet in enumerate( jets ):
            jet[ kwargs[ "overflapFlagName" ] ] = not jetsAlive[ jetIdx ]

    if strategy == "deltaR_lep_jet_conditional":
        argsNeeded = [ 
            "deltaRThreshold", 
            "leptonPtThreshold",
            "jetPtThreshold",
            "conditionalFunc",
            "overflapFlagName"
        ]
        for neededArg in argsNeeded:
            if neededArg not in kwargs.keys():
                logger.error( f"Please provide the `{neededArg}` parameter for the `deltaR_sequential` strategy of overlap removal." )
                return leptons, jets
        deltaRFunc = lambda vec1, vec2: vec1.deltaR( vec2 )
        if kwargs.get( "customDeltaRFunc", None ) is not None:
            deltaRFunc = kwargs[ "customDeltaRFunc" ]
        closestJetIdxForLeptons    = []
        closestJetDeltaRForLeptons = []
        closestLeptonIdxForJets    = []
        closestLeptonDeltaRForJets = []
        leptonVecs = [ vector.MomentumObject3D.from_rhophieta( lepton[ "pt" ], lepton[ "phi" ], lepton[ "eta" ] ) for lepton in leptons ]
        jetVecs    = [ vector.MomentumObject3D.from_rhophieta( jet[ "pt" ], jet[ "phi" ], jet[ "eta" ] ) for jet in jets ]
        leptonsAlive = np.array( [ lep[ "pt" ] > kwargs[ "leptonPtThreshold" ] for lep in leptons ] )
        jetsAlive    = np.array( [ jet[ "pt" ] > kwargs[ "jetPtThreshold" ] for jet in jets ] )
        for leptonIdx, leptonVec in enumerate( leptonVecs ):
            if not leptonsAlive[ leptonIdx ]:
                closestJetIdxForLeptons.append( np.inf )
                closestJetDeltaRForLeptons.append( np.inf )
                continue
            closestJetIdx = np.inf
            closestJetDeltaR = np.inf
            for jetIdx, jetVec in enumerate( jetVecs ):
                if not jetsAlive[ jetIdx ]:
                    continue
                deltaR = deltaRFunc( leptonVec, jetVec )
                if deltaR < closestJetDeltaR:
                    closestJetDeltaR = deltaR
                    closestJetIdx = jetIdx
            closestJetIdxForLeptons.append( closestJetIdx )
            closestJetDeltaRForLeptons.append( closestJetDeltaR )
        closestJetIdxForLeptons = np.array( closestJetIdxForLeptons )
        closestJetDeltaRForLeptons = np.array( closestJetDeltaRForLeptons )
        jetIdxMarkedForRemoval = closestJetIdxForLeptons[ np.array( closestJetDeltaRForLeptons < kwargs[ "deltaRThreshold" ] ) ]
        isJetConditionedToRemove = np.array( [ kwargs[ "conditionalFunc" ]( jet ) for jet in jets ] )
        jetIdxMarkedForRemoval = np.array( [ jetIdx for jetIdx in jetIdxMarkedForRemoval if isJetConditionedToRemove[ jetIdx ] ] )
        if len( jetIdxMarkedForRemoval ) > 0:
            jetsAlive[ jetIdxMarkedForRemoval ] = False
        lepIdxMarkedForRemoval = np.array( closestJetDeltaRForLeptons < kwargs[ "deltaRThreshold" ] ) 
        if len( closestJetIdxForLeptons ) > 0:
            lepIdxMarkedForRemoval = lepIdxMarkedForRemoval & ~isJetConditionedToRemove[ np.where( np.isinf( closestJetIdxForLeptons ), 0, closestJetIdxForLeptons ).astype(int) ]
        if len( lepIdxMarkedForRemoval ) > 0:
            leptonsAlive[ lepIdxMarkedForRemoval ] = False
        for lepIdx, lep in enumerate( leptons ):
            lep[ kwargs[ "overflapFlagName" ] ] = not leptonsAlive[ lepIdx ]
        for jetIdx, jet in enumerate( jets ):
            jet[ kwargs[ "overflapFlagName" ] ] = not jetsAlive[ jetIdx ]

    return jets, leptons

def addLocalVariables( objects, variables ):
    if not isinstance( variables, list ):
        variables = [ variables ]
    for variable in variables:
        if variable == "z0SinTheta":
            for obj in objects:
                objvec = vector.MomentumObject3D.from_rhophieta( obj[ "pt" ], obj[ "phi" ], obj[ "eta" ] )
                obj[ variable ] = obj[ "z0" ] * np.sin( objvec.theta )
    return objects         

def getMissingPz( visible, missing, parent_mass = constants.masses[ "W" ] ):
    """
    Calculate the z-component of the missing momentum under the 
    following assumptions:
    - Parent -> visible + invisible
        - We know the 4-momentum of the visible system.
            - The visible system can be a multi-particle system.
        - We know the xy momentum components of the invisible particle.
        - Invisible particle is massless.
        - The parent particle is on-shell, i.e., it's mass is known.
        - TODO: Verify the understanding that the assumption that the xy 
        momentum components of the invisible particle are known can be 
        fullfilled only under the assumption that the measured missing 
        xy momentum is due to the invisible particle.
    """
    visible_pt   = visible[ "pt"   ]
    visible_phi  = visible[ "phi"  ]
    visible_eta  = visible[ "eta"  ]
    visible_mass = visible[ "mass" ]
    missing_et   = missing[ "et"   ]
    missing_phi  = missing[ "phi"  ]

    visible_p4 = vector.MomentumObject4D.from_rhophietatau( 
                    visible_pt, 
                    visible_phi, 
                    visible_eta, 
                    visible_mass 
                )
    visible_pz = visible_p4.z
    visible_pt = vector.MomentumObject2D.from_rhophi( visible_pt, visible_phi )
    missing_pt = vector.MomentumObject2D.from_rhophi( missing_et, missing_phi )
    visible_e  = visible_p4.t

    # Follows from straightforward relativistic kinematics:
    # See, for example: https://arxiv.org/pdf/2008.05316 (Eq. 2). 
    # We generalize it to the case where the visible system is not massless.
    termA = visible_pz ** 2 - visible_e ** 2
    termB = visible_pz * ( 2 * visible_pt.dot( missing_pt ) + parent_mass ** 2 - visible_mass ** 2 )
    termC = ( parent_mass ** 2 - visible_mass ** 2 ) ** 2 / 4 \
            + ( visible_pt.dot( missing_pt ) ) ** 2 \
            + ( visible_pt.dot( missing_pt ) ) * ( parent_mass ** 2 - visible_mass ** 2 ) \
            - ( ( visible_e ** 2 ) * ( missing_pt ** 2 ) )
    
    discriminant = termB ** 2 - 4 * termA * termC
    if discriminant < 0:
        solutions = np.array( [ np.nan, np.nan ] )
    else: 
        solutions = np.array( 
            [ 
                ( -termB + np.sqrt( discriminant ) ) / ( 2 * termA ), 
                ( -termB - np.sqrt( discriminant ) ) / ( 2 * termA ) 
            ] 
        )
    return solutions

def getTransParentMass( visible, missing ):
    visible_pt   = visible[ "pt"  ]
    visible_phi  = visible[ "phi" ]
    missing_et   = missing[ "et"  ]
    missing_phi  = missing[ "phi" ]
    visible      = vector.MomentumObject2D.from_rhophi( visible_pt, visible_phi )
    missing      = vector.MomentumObject2D.from_rhophi( missing_et, missing_phi )
    deltaPhi     = visible.deltaphi( missing )
    mTW = np.sqrt( 2 * visible_pt * missing_et * ( 1 - np.cos( deltaPhi ) ) )
    return mTW

def getLepWCandidates( l, mTWThreshold = 0., metPhiVariationThreshold = np.pi / 4 ):
    # TODO: Add variation of MET phi to get the Neutrino pz solutions,
    #       to account for the MET resolution. Can add here or in the
    #       `getMissingPz` function.
    Leptons = l[ "Lepton"    ]
    met     = l[ "MissingET" ] 
    def getLepWCandidate( 
            Lepton, 
            met, 
            mTW          = None, 
            mTWThreshold = mTWThreshold,
            LepIdx       = -1 
        ):
        Ws = []
        phiOrig = met[ "phi" ]
        gotPz = False
        if mTW is None:
            mTW = getTransParentMass( Lepton, met )
        if mTW < mTWThreshold:
            return Ws
        Neutrino_pz_sols = getMissingPz( Lepton, met )
        Neutrino_pz_sols = Neutrino_pz_sols[ ~np.isnan( Neutrino_pz_sols ) ]
        if len( Neutrino_pz_sols ) > 0:
            gotPz = True
        if not gotPz:
            met = copy.deepcopy( met )
            for metPhiVariation in np.linspace( 0, metPhiVariationThreshold, 100 ):
                for varSign in [ 1, -1 ]:
                    met[ "phi" ] = phiOrig + varSign * metPhiVariation
                    Neutrino_pz_sols = getMissingPz( Lepton, met )
                    Neutrino_pz_sols = Neutrino_pz_sols[ ~np.isnan( Neutrino_pz_sols ) ]
                    if len( Neutrino_pz_sols ) > 0:
                        gotPz = True
                    if gotPz:
                        break
                if gotPz:
                    break
        deltaPhi = met[ "phi" ] - phiOrig
        for Neutrino_pz in Neutrino_pz_sols:
            Neutrino_vec = vector.MomentumObject4D.from_rhophiztau( 
                                met[ "et"  ], 
                                met[ "phi" ], 
                                Neutrino_pz, 
                                0. 
                           )
            Lepton_vec   = vector.MomentumObject4D.from_rhophietatau( 
                                Lepton[ "pt"   ], 
                                Lepton[ "phi"  ], 
                                Lepton[ "eta"  ], 
                                Lepton[ "mass" ] 
                           )
            W_vec        = Lepton_vec + Neutrino_vec
            W = {
                "LepW_pt"                  : W_vec.rho,
                "LepW_eta"                 : W_vec.eta,
                "LepW_phi"                 : W_vec.phi,
                "LepW_mass"                : W_vec.tau,
                "LepW_px"                  : W_vec.x,
                "LepW_py"                  : W_vec.y,
                "LepW_pz"                  : W_vec.z,
                "LepW_e"                   : W_vec.t,
                "LepW_p"                   : np.sqrt( W_vec.x ** 2 + W_vec.y ** 2 + W_vec.z ** 2 ),
                "LepW_charge"              : Lepton[ "charge" ],
                "LepW_trasnverse_mass"     : mTW,
                "Lepton_pt"                : Lepton_vec.rho,
                "Lepton_eta"               : Lepton_vec.eta,
                "Lepton_phi"               : Lepton_vec.phi,
                "Lepton_mass"              : Lepton["mass"],
                "Lepton_px"                : Lepton_vec.x,
                "Lepton_py"                : Lepton_vec.y,
                "Lepton_pz"                : Lepton_vec.z,
                "Lepton_e"                 : Lepton_vec.t,
                "Lepton_p"                 : np.sqrt( Lepton_vec.x ** 2 + Lepton_vec.y ** 2 + Lepton_vec.z ** 2 ),
                "Lepton_charge"            : Lepton[ "charge" ],
                "Lepton_isoCone10"         : Lepton[ "trackPtIsoCone10" ],
                "Lepton_isoCone20"         : Lepton[ "trackPtIsoCone20" ],
                "Lepton_isoCone30"         : Lepton[ "trackPtIsoCone30" ],
                "Lepton_isoConeCustom10"   : Lepton[ "trackPtIsoCustomCone10" ],
                "Lepton_isoConeCustom20"   : Lepton[ "trackPtIsoCustomCone20" ],
                "Lepton_isoConeCustom30"   : Lepton[ "trackPtIsoCustomCone30" ],
                "Lepton_d0"                : Lepton[ "d0" ],
                "Lepton_d0_err"            : Lepton[ "d0_err" ],
                "Lepton_z0"                : Lepton[ "z0" ],
                "Lepton_z0_err"            : Lepton[ "z0_err" ],
                #"Lepton_d0_sig"            : Lepton[ "d0" ] / Lepton[ "d0_err" ],
                #"Lepton_z0_sig"            : Lepton[ "z0" ] / Lepton[ "z0_err" ],
                "Lepton_minDeltaRWithJets" : Lepton[ "minDeltaRWithJets" ],
                "Lepton_z0sinTheta"        : Lepton[ "z0" ] * np.sin( Lepton_vec.theta ),
                "Lepton_idx"               : LepIdx,
                "Neutrino_pt"              : Neutrino_vec.rho,
                "Neutrino_eta"             : Neutrino_vec.eta,
                "Neutrino_phi"             : Neutrino_vec.phi,
                "Neutrino_mass"            : 0.,
                "Neutrino_px"              : Neutrino_vec.x,
                "Neutrino_py"              : Neutrino_vec.y,
                "Neutrino_pz"              : Neutrino_vec.z,
                "Neutrino_e"               : Neutrino_vec.t,
                "Neutrino_p"               : np.sqrt( Neutrino_vec.x ** 2 + Neutrino_vec.y ** 2 + Neutrino_vec.z ** 2 ),
                "Neutrino_phi_variation"   : deltaPhi,
                "Neutrino_phi_original"    : phiOrig,
                "deltaR_Lepton_Neutrino"   : Lepton_vec.deltaR     ( Neutrino_vec ),
                "deltaPhi_Lepton_Neutrino" : Lepton_vec.deltaphi   ( Neutrino_vec ),
                "deltaR_Lepton_LepW"       : Lepton_vec.deltaR     ( W_vec        ),
                "deltaPhi_Lepton_LepW"     : Lepton_vec.deltaphi   ( W_vec        ),
                "deltaR_Neutrino_LepW"     : Neutrino_vec.deltaR   ( W_vec        ),
                "deltaPhi_Neutrino_LepW"   : Neutrino_vec.deltaphi ( W_vec        )
            }
            Ws.append( W )
        return Ws
    Ws = []
    for LepIdx, Lepton in enumerate( Leptons ):
        Ws += getLepWCandidate( Lepton, met, None, mTWThreshold, LepIdx )
    return np.array( Ws )

def getHadWCandidates( l, vetoBTagged = True, bTagThreshold = 1.0 ):
    jets = l[ "Jet" ]
    def getHadWCandidate( 
            Jet0, 
            Jet1, 
            vetoBTagged   = vetoBTagged,
            bTagThreshold = bTagThreshold,
            Jet0Idx       = -1,
            Jet1Idx       = -1, 
        ):
        Ws = []
        if vetoBTagged:
            if Jet0[ "btag" ] > bTagThreshold or Jet1[ "btag" ] > bTagThreshold:
                return Ws
        if Jet0[ "pt" ] < Jet1[ "pt" ]:
            Jet0, Jet1       = Jet1, Jet0
            Jet0Idx, Jet1Idx = Jet1Idx, Jet0Idx
        Jet0_vec = vector.VectorObject4D.from_rhophietatau( Jet0[ "pt" ], Jet0[ "phi" ], Jet0[ "eta" ], Jet0[ "mass" ] )
        Jet1_vec = vector.VectorObject4D.from_rhophietatau( Jet1[ "pt" ], Jet1[ "phi" ], Jet1[ "eta" ], Jet1[ "mass" ] )
        W_vec = Jet0_vec + Jet1_vec
        W = {
                "HadW_pt"            : W_vec.rho,
                "HadW_eta"           : W_vec.eta,
                "HadW_phi"           : W_vec.phi,
                "HadW_mass"          : W_vec.tau,
                "HadW_px"            : W_vec.x,
                "HadW_py"            : W_vec.y,
                "HadW_pz"            : W_vec.z,
                "HadW_e"             : W_vec.t,
                "HadW_p"             : np.sqrt( W_vec.x ** 2 + W_vec.y ** 2 + W_vec.z ** 2 ),
                "Jet0_pt"            : Jet0_vec.rho,
                "Jet0_eta"           : Jet0_vec.eta,
                "Jet0_phi"           : Jet0_vec.phi,
                "Jet0_mass"          : Jet0_vec.tau,
                "Jet0_px"            : Jet0_vec.x,
                "Jet0_py"            : Jet0_vec.y,
                "Jet0_pz"            : Jet0_vec.z,
                "Jet0_e"             : Jet0_vec.t,
                "Jet0_p"             : np.sqrt( Jet0_vec.x ** 2 + Jet0_vec.y ** 2 + Jet0_vec.z ** 2 ),
                "Jet0_charge"        : Jet0[ "charge" ],
                "Jet0_btag"          : Jet0[ "btag"   ],
                "Jet0_minDeltaRWithElectrons" : Jet0[ "minDeltaRWithElectrons" ],
                "Jet0_minDeltaRWithMuons" : Jet0[ "minDeltaRWithMuons" ],
                "Jet1_pt"            : Jet1_vec.rho,
                "Jet1_eta"           : Jet1_vec.eta,
                "Jet1_phi"           : Jet1_vec.phi,
                "Jet1_mass"          : Jet1_vec.tau,
                "Jet1_px"            : Jet1_vec.x,
                "Jet1_py"            : Jet1_vec.y,
                "Jet1_pz"            : Jet1_vec.z,
                "Jet1_e"             : Jet1_vec.t,
                "Jet1_p"             : np.sqrt( Jet1_vec.x ** 2 + Jet1_vec.y ** 2 + Jet1_vec.z ** 2 ),
                "Jet1_charge"        : Jet1[ "charge" ],
                "Jet1_btag"          : Jet1[ "btag"   ],
                "Jet1_minDeltaRWithElectrons" : Jet1[ "minDeltaRWithElectrons" ],
                "Jet1_minDeltaRWithMuons" : Jet1[ "minDeltaRWithMuons" ],
                "deltaR_Jet0_Jet1"   : Jet0_vec.deltaR   ( Jet1_vec ),
                "deltaPhi_Jet0_Jet1" : Jet0_vec.deltaphi ( Jet1_vec ),
                "deltaR_Jet0_HadW"   : Jet0_vec.deltaR   ( W_vec    ),
                "deltaPhi_Jet0_HadW" : Jet0_vec.deltaphi ( W_vec    ),
                "deltaR_Jet1_HadW"   : Jet1_vec.deltaR   ( W_vec    ),
                "deltaPhi_Jet1_HadW" : Jet1_vec.deltaphi ( W_vec    ),
                "Jet0_idx"           : Jet0Idx,
                "Jet1_idx"           : Jet1Idx
            }
        Ws.append( W )
        return Ws
    Ws = []
    for Jet0Idx, Jet0 in enumerate( jets ):
        for Jet1Idx, Jet1 in enumerate( jets ):
            if Jet0Idx >= Jet1Idx:
                continue
            Ws += getHadWCandidate( Jet0, Jet1, vetoBTagged, bTagThreshold, Jet0Idx, Jet1Idx )
    return np.array( Ws )

def getLepTopCandidates( 
        l, 
        requireBTagged = False, 
        bTagThreshold  = 1.0
    ):
    bJets          = l[ "Jet"  ]
    LepWCandidates = l[ "LepW" ]
    def getLepTopCandidate( 
            LepW, 
            bJet, 
            requireBTagged = requireBTagged,
            bTagThreshold  = bTagThreshold,
            LepWIdx        = -1,
            bJetIdx        = -1
        ):
        tops = []
        if requireBTagged and bJet[ "btag" ] < bTagThreshold:
            return tops 
        LepW_vec      = vector.VectorObject4D.from_rhophietatau( 
                            LepW[ "LepW_pt"   ], 
                            LepW[ "LepW_phi"  ], 
                            LepW[ "LepW_eta"  ], 
                            LepW[ "LepW_mass" ] 
                        )
        bJet_vec      = vector.VectorObject4D.from_rhophietatau( 
                            bJet[ "pt"   ], 
                            bJet[ "phi"  ], 
                            bJet[ "eta"  ], 
                            bJet[ "mass" ] 
                        )
        Lepton_vec   = vector.VectorObject4D.from_rhophietatau(
                            LepW[ "Lepton_pt"   ],
                            LepW[ "Lepton_phi"  ],
                            LepW[ "Lepton_eta"  ],
                            LepW[ "Lepton_mass" ]
                       )
        Neutrino_vec = vector.VectorObject4D.from_rhophietatau(
                            LepW[ "Neutrino_pt"   ],
                            LepW[ "Neutrino_phi"  ],
                            LepW[ "Neutrino_eta"  ],
                            LepW[ "Neutrino_mass" ]
                       )
        top_vec  = LepW_vec + bJet_vec
        top = {
                "LepTop_pt"                     : top_vec.rho,
                "LepTop_eta"                    : top_vec.eta,
                "LepTop_phi"                    : top_vec.phi,
                "LepTop_mass"                   : top_vec.tau,
                "LepTop_px"                     : top_vec.x,
                "LepTop_py"                     : top_vec.y,
                "LepTop_pz"                     : top_vec.z,
                "LepTop_e"                      : top_vec.t,
                "LepTop_p"                      : np.sqrt( top_vec.x ** 2 + top_vec.y ** 2 + top_vec.z ** 2 ),
                "LepTopbJet_minDeltaRWithElectrons" : bJet[ "minDeltaRWithElectrons" ],
                "LepTopbJet_minDeltaRWithMuons" : bJet[ "minDeltaRWithMuons" ],
                "LepTopbJet_pt"                 : bJet_vec.rho,
                "LepTopbJet_eta"                : bJet_vec.eta,
                "LepTopbJet_phi"                : bJet_vec.phi,
                "LepTopbJet_mass"               : bJet_vec.tau,
                "LepTopbJet_px"                 : bJet_vec.x,
                "LepTopbJet_py"                 : bJet_vec.y,
                "LepTopbJet_pz"                 : bJet_vec.z,
                "LepTopbJet_e"                  : bJet_vec.t,
                "LepTopbJet_p"                  : np.sqrt( bJet_vec.x ** 2 + bJet_vec.y ** 2 + bJet_vec.z ** 2 ),
                "LepTopbJet_btag"               : bJet[ "btag"   ],
                "LepTopbJet_charge"             : bJet[ "charge" ],
                "deltaR_LepW_LepTopbJet"        : LepW_vec.deltaR       ( bJet_vec ),
                "deltaPhi_LepW_LepTopbJet"      : LepW_vec.deltaphi     ( bJet_vec ),
                "deltaR_LepW_LepTop"            : LepW_vec.deltaR       ( top_vec  ),
                "deltaPhi_LepW_LepTop"          : LepW_vec.deltaphi     ( top_vec  ),
                "deltaR_LepTopbJet_LepTop"      : bJet_vec.deltaR       ( top_vec  ),
                "deltaPhi_LepTopbJet_LepTop"    : bJet_vec.deltaphi     ( top_vec  ),
                "deltaR_Lepton_LepTopbJet"      : Lepton_vec.deltaR     ( bJet_vec ),
                "deltaR_Lepton_LepTop"          : Lepton_vec.deltaR     ( top_vec  ),
                "deltaR_Neutrino_LepTpopbJet"   : Neutrino_vec.deltaR   ( bJet_vec ),
                "deltaR_Neutrino_LepTop"        : Neutrino_vec.deltaR   ( top_vec  ),
                "deltaPhi_Lepton_LepTopbJet"    : Lepton_vec.deltaphi   ( bJet_vec ),
                "deltaPhi_Lepton_LepTop"        : Lepton_vec.deltaphi   ( top_vec  ),
                "deltaPhi_Neutrino_LepTpopbJet" : Neutrino_vec.deltaphi ( bJet_vec ),
                "deltaPhi_Neutrino_LepTop"      : Neutrino_vec.deltaphi ( top_vec  ),
                "LepW_idx"                      : LepWIdx,
                "LepTopbJet_idx"                : bJetIdx
        }
        top.update( LepW )
        tops.append( top )
        return tops 
    tops = []
    for LepWIdx, LepW in enumerate( LepWCandidates ):
        for bJetIdx, bJet in enumerate( bJets ):
            tops += getLepTopCandidate( LepW, bJet, requireBTagged, bTagThreshold, LepWIdx, bJetIdx )
    return np.array( tops )

def getHadTopCandidates(
        l,
        requireBTagged = False,
        bTagThreshold  = 1.0
    ):
    hadWCandidates = l[ "HadW" ]
    bJets          = l[ "Jet" ]
    def getHadTopCandidate(
            hadW,
            bJet,
            requireBTagged = requireBTagged,
            bTagThreshold  = bTagThreshold,
            hadWIdx        = -1,
            bJetIdx        = -1
        ):
        tops = []
        if requireBTagged and bJet[ "btag" ] < bTagThreshold:
            return tops
        hadW_vec = vector.VectorObject4D.from_rhophietatau( 
                        hadW[ "HadW_pt"   ], 
                        hadW[ "HadW_phi"  ], 
                        hadW[ "HadW_eta"  ], 
                        hadW[ "HadW_mass" ] 
                   )
        bJet_vec = vector.VectorObject4D.from_rhophietatau( 
                        bJet[ "pt"   ], 
                        bJet[ "phi"  ], 
                        bJet[ "eta"  ], 
                        bJet[ "mass" ] 
                   )
        Jet0_vec = vector.VectorObject4D.from_rhophietatau(
                        hadW[ "Jet0_pt"   ],
                        hadW[ "Jet0_eta"  ],
                        hadW[ "Jet0_phi"  ],
                        hadW[ "Jet0_mass" ]
        ) 
        Jet1_vec = vector.VectorObject4D.from_rhophietatau(
                        hadW[ "Jet1_pt"   ],
                        hadW[ "Jet1_eta"  ],
                        hadW[ "Jet1_phi"  ],
                        hadW[ "Jet1_mass" ]
        ) 
        top_vec  = hadW_vec + bJet_vec
        top = {
                "HadTop_pt"                  : top_vec.rho,
                "HadTop_eta"                 : top_vec.eta,
                "HadTop_phi"                 : top_vec.phi,
                "HadTop_mass"                : top_vec.tau,
                "HadTop_px"                  : top_vec.x,
                "HadTop_py"                  : top_vec.y,
                "HadTop_pz"                  : top_vec.z,
                "HadTop_e"                   : top_vec.t,
                "HadTop_p"                   : np.sqrt( top_vec.x ** 2 + top_vec.y ** 2 + top_vec.z ** 2 ),
                "HadTopbJet_minDeltaRWithElectrons" : bJet[ "minDeltaRWithElectrons" ],
                "HadTopbJet_minDeltaRWithMuons" : bJet[ "minDeltaRWithMuons" ],
                "HadTopbJet_pt"              : bJet_vec.rho,
                "HadTopbJet_eta"             : bJet_vec.eta,
                "HadTopbJet_phi"             : bJet_vec.phi,
                "HadTopbJet_mass"            : bJet_vec.tau,
                "HadTopbJet_px"              : bJet_vec.x,
                "HadTopbJet_py"              : bJet_vec.y,
                "HadTopbJet_pz"              : bJet_vec.z,
                "HadTopbJet_e"               : bJet_vec.t,
                "HadTopbJet_p"               : np.sqrt( bJet_vec.x ** 2 + bJet_vec.y ** 2 + bJet_vec.z ** 2 ),
                "HadTopbJet_charge"          : bJet[ "charge" ],
                "HadTopbJet_btag"            : bJet[ "btag"   ],
                "deltaR_HadW_HadTopbJet"     : hadW_vec.deltaR   ( bJet_vec ),
                "deltaPhi_HadW_HadTopbJet"   : hadW_vec.deltaphi ( bJet_vec ),
                "deltaR_HadW_HadTop"         : hadW_vec.deltaR   ( top_vec  ),
                "deltaPhi_HadW_HadTop"       : hadW_vec.deltaphi ( top_vec  ),
                "deltaR_HadTopbJet_HadTop"   : bJet_vec.deltaR   ( top_vec  ),
                "deltaPhi_HadTopbJet_HadTop" : bJet_vec.deltaphi ( top_vec  ),
                "deltaR_Jet0_HadTopbJet"     : Jet0_vec.deltaR   ( bJet_vec ),
                "deltaR_Jet0_HadTop"         : Jet0_vec.deltaR   ( top_vec  ),
                "deltaR_Jet1_HadTopbJet"     : Jet1_vec.deltaR   ( bJet_vec ),
                "deltaR_Jet1_HadTop"         : Jet1_vec.deltaR   ( top_vec  ),
                "deltaPhi_Jet0_HadTopbJet"   : Jet0_vec.deltaphi ( bJet_vec ),
                "deltaPhi_Jet0_HadTop"       : Jet0_vec.deltaphi ( top_vec  ),
                "deltaPhi_Jet1_HadTopbJet"   : Jet1_vec.deltaphi ( bJet_vec ),
                "deltaPhi_Jet1_HadTop"       : Jet1_vec.deltaphi ( top_vec  ),
                "HadW_idx"                   : hadWIdx,
                "HadTopbJet_idx"             : bJetIdx
        }
        top.update( hadW )
        tops.append( top )
        return tops
    tops = []
    for hadWIdx, hadW in enumerate( hadWCandidates ):
        for bJetIdx, bJet in enumerate( bJets ):
            if hadW[ "Jet0_idx" ] == bJetIdx:
                continue
            if hadW[ "Jet1_idx" ] == bJetIdx:
                continue
            tops += getHadTopCandidate( hadW, bJet, requireBTagged, bTagThreshold, hadWIdx, bJetIdx )
    return np.array( tops )

def getTTbarCandidates( 
        l, 
        requireLepBTagged = False, 
        requireHadBTagged = False, 
        bTagThreshold     = 1.0,
        logger            = default_logger 
    ):
    LepTopCandidates = l[ "LepTop" ]
    hadTopCandidates = l[ "HadTop" ]
    def getTTbarCandidate( 
            LepTop, 
            hadTop, 
            requireLepBTagged = requireLepBTagged,
            requireHadBTagged = requireHadBTagged,
            bTagThreshold     = bTagThreshold,
            LepTopIdx         = -1, 
            hadTopIdx         = -1 
        ):
        if requireLepBTagged and LepTop[ "LepTopbJet_btag" ] < bTagThreshold:
            #logger.debug( f"Skipping LepTop candidate {LepTopIdx} due to b-tagging requirement." )
            return []
        if requireHadBTagged and hadTop[ "HadTopbJet_btag" ] < bTagThreshold:
            #logger.debug( f"Skipping hadTop candidate {hadTopIdx} due to b-tagging requirement." )
            return [] 
        LepTopVec   = vector.VectorObject4D.from_rhophietatau( LepTop[ "LepTop_pt"   ], LepTop[ "LepTop_phi"   ], LepTop[ "LepTop_eta"   ], LepTop[ "LepTop_mass"   ] )
        hadTopVec   = vector.VectorObject4D.from_rhophietatau( hadTop[ "HadTop_pt"   ], hadTop[ "HadTop_phi"   ], hadTop[ "HadTop_eta"   ], hadTop[ "HadTop_mass"   ] )
        LepWVec     = vector.VectorObject4D.from_rhophietatau( LepTop[ "LepW_pt"     ], LepTop[ "LepW_phi"     ], LepTop[ "LepW_eta"     ], LepTop[ "LepW_mass"     ] )
        hadWVec     = vector.VectorObject4D.from_rhophietatau( hadTop[ "HadW_pt"     ], hadTop[ "HadW_phi"     ], hadTop[ "HadW_eta"     ], hadTop[ "HadW_mass"     ] )
        Jet0Vec     = vector.VectorObject4D.from_rhophietatau( hadTop[ "Jet0_pt"     ], hadTop[ "Jet0_phi"     ], hadTop[ "Jet0_eta"     ], hadTop[ "Jet0_mass"     ] )
        Jet1Vec     = vector.VectorObject4D.from_rhophietatau( hadTop[ "Jet1_pt"     ], hadTop[ "Jet1_phi"     ], hadTop[ "Jet1_eta"     ], hadTop[ "Jet1_mass"     ] )
        NeutrinoVec = vector.VectorObject4D.from_rhophietatau( LepTop[ "Neutrino_pt" ], LepTop[ "Neutrino_phi" ], LepTop[ "Neutrino_eta" ], LepTop[ "Neutrino_mass" ] )
        LeptonVec   = vector.VectorObject4D.from_rhophietatau( LepTop[ "Lepton_pt"   ], LepTop[ "Lepton_phi"   ], LepTop[ "Lepton_eta"   ], LepTop[ "Lepton_mass"   ] )
        LepTopbJetVec = vector.VectorObject4D.from_rhophietatau( 
            LepTop[ "LepTopbJet_pt"   ],
            LepTop[ "LepTopbJet_phi"  ],
            LepTop[ "LepTopbJet_eta"  ],
            LepTop[ "LepTopbJet_mass" ]
        )
        hadTopbJetVec = vector.VectorObject4D.from_rhophietatau( 
            hadTop[ "HadTopbJet_pt"   ],
            hadTop[ "HadTopbJet_phi"  ],
            hadTop[ "HadTopbJet_eta"  ],
            hadTop[ "HadTopbJet_mass" ]
        )
        ttbarVec    = LepTopVec + hadTopVec
        ttbar = {
                "ttbar_pt"               : ttbarVec.rho,
                "ttbar_eta"              : ttbarVec.eta,
                "ttbar_phi"              : ttbarVec.phi,
                "ttbar_mass"             : ttbarVec.tau, 
                "ttbar_px"               : ttbarVec.x,
                "ttbar_py"               : ttbarVec.y,
                "ttbar_pz"               : ttbarVec.z,
                "ttbar_e"                : ttbarVec.t, 
                "ttbar_p"                : np.sqrt( ttbarVec.x ** 2 + ttbarVec.y ** 2 + ttbarVec.z ** 2 ),
                "LepTop_idx"             : LepTopIdx,
                "hadTop_idx"             : hadTopIdx,
            }
        LepTopItemsDict = {
            "LepTop"     : LepTopVec,
            "LepW"       : LepWVec,
            "LepTopbJet" : LepTopbJetVec,
            "Lepton"     : LeptonVec,
            "Neutrino"   : NeutrinoVec 
        }
        HadTopItemsDict = {
            "HadTop"     : hadTopVec,
            "HadW"       : hadWVec,
            "HadTopbJet" : hadTopbJetVec,
            "Jet0"       : Jet0Vec,
            "Jet1"       : Jet1Vec
        }
        angularCombos = {}
        for lepTopItemName, lepTopItemVec in LepTopItemsDict.items():
            for hadTopItemName, hadTopItemVec in HadTopItemsDict.items():
                angularCombos[ f"deltaR_{lepTopItemName}_{hadTopItemName}"   ] = lepTopItemVec.deltaR   ( hadTopItemVec )
                angularCombos[ f"deltaPhi_{lepTopItemName}_{hadTopItemName}" ] = lepTopItemVec.deltaphi ( hadTopItemVec )
        ttbar.update( LepTop )
        ttbar.update( hadTop )
        ttbar.update( { "ttbar_chi2" : getTTBarKinematicChi2( ttbar ) } )
        return [ ttbar ]
    ttbarCandidates = []
    for LepTopIdx, LepTop in enumerate( LepTopCandidates ):
        for hadTopIdx, hadTop in enumerate( hadTopCandidates ):
            hadTopJetIdxs = [ hadTop[ "Jet0_idx" ], hadTop[ "Jet1_idx" ], hadTop[ "HadTopbJet_idx" ] ]
            if LepTop[ "LepTopbJet_idx" ] in hadTopJetIdxs:
                continue
            ttbarCandidates += getTTbarCandidate( LepTop, hadTop, requireLepBTagged, requireHadBTagged, bTagThreshold, LepTopIdx, hadTopIdx )
            if len( ttbarCandidates ) == 0:
                logger.debug( f"Failed to find a ttbar candidate for LepTopIdx = {LepTopIdx} and hadTopIdx = {hadTopIdx} despite no jet overlap!" )
    return np.array( ttbarCandidates )

def getTTBarKinematicChi2( 
        ttbar,
        mW                       = constants.masses[ "W"   ],
        mTopMinusW               = constants.masses[ "top" ] - constants.masses[ "W" ],
        mTop                     = constants.masses[ "top" ],
        widthW                   = 12.07,
        widthTopMinusW           = 16.05,
        widthTop                 = 25.41,
        pTImbalanceExpected      = 0.23,
        pTImbalanceExpectedWidth = 18.85
):
    # TODO: Determine the paramters of this chi2 function based on our 
    #       own MC samples. Need MC samples with truth-matched Zp->ttbar
    #       candidates.

    chi2 = 0
    # Hadronic W Mass Cost 
    chi2 += ( ttbar[ "HadW_mass" ] - mW ) ** 2 / widthW ** 2
    # Hadronic Top Mass Cost
    chi2 += ( ttbar[ "HadTop_mass" ] - ttbar[ "HadW_mass" ] - mTopMinusW ) ** 2 / widthTopMinusW ** 2
    # Leptonic Top Mass Cost
    chi2 += ( ttbar[ "LepTop_mass" ] - mTop ) ** 2 / widthTop ** 2
    # pT Imbalance Cost
    chi2 += ( ( ttbar[ "LepTop_pt" ] - ttbar[ "HadTop_pt" ] - pTImbalanceExpected ) ** 2 ) / ( pTImbalanceExpectedWidth ** 2 )
    return chi2

def card( df, dsid = None, logger = None, **kwargs ):
    if logger is None:
        logger = logging.get_logger(__name__)
    if "candidateBuildingParameters" not in kwargs.keys():
        logger.critical(
            "`candidateBuildingParameters` is needed for `ttbarRecoSemiLepResolved` physicsAna card. Update your ntuples config!"
        )
        raise ValueError(
            "`candidateBuildingParameters` is needed for `ttbarRecoSemiLepResolved` physicsAna card. Update your ntuples config!"
        )
    
    cutflowLenDicts = {
        "numEvents"               : lambda x: len( x ),
        "numEventsWith1Electron"  : lambda x: len( x[ x[ "Electron" ].apply( lambda y: len( y ) ) == 1 ] ),
        "numEventsWith2Electrons" : lambda x: len( x[ x[ "Electron" ].apply( lambda y: len( y ) ) == 2 ] ),
        "numEventsWith>2Electrons": lambda x: len( x[ x[ "Electron" ].apply( lambda y: len( y ) ) > 2 ] ),
        "numElectrons"            : lambda x: sum(    x[ "Electron" ].apply( lambda y: len( y ) ) ),
        "numEventsWith1Muon"      : lambda x: len( x[ x[ "Muon" ].apply( lambda y: len( y ) ) == 1 ] ),
        "numEventsWith2Muons"     : lambda x: len( x[ x[ "Muon" ].apply( lambda y: len( y ) ) == 2 ] ),
        "numEventsWith>2Muons"    : lambda x: len( x[ x[ "Muon" ].apply( lambda y: len( y ) ) > 2 ] ),
        "numMuons"                : lambda x: sum(    x[ "Muon" ].apply( lambda y: len( y ) ) ),
        "numEventsWith1Jet"       : lambda x: len( x[ x[ "Jet" ].apply( lambda y: len( y ) ) == 1 ] ),
        "numEventsWith2Jets"      : lambda x: len( x[ x[ "Jet" ].apply( lambda y: len( y ) ) == 2 ] ),
        "numEventsWith3Jets"      : lambda x: len( x[ x[ "Jet" ].apply( lambda y: len( y ) ) == 3 ] ),
        "numEventsWith4Jets"      : lambda x: len( x[ x[ "Jet" ].apply( lambda y: len( y ) ) == 4 ] ),
        "numEventsWith>4Jets"     : lambda x: len( x[ x[ "Jet" ].apply( lambda y: len( y ) ) > 4 ] ),
        "numJets"                 : lambda x: sum( x[ "Jet" ].apply( lambda y: len( y ) ) ),
    }
    cutflow = utils.cutFlower( df, cutflowLenDicts )

    derConfig = kwargs[ "candidateBuildingParameters" ]
    logger.debug( "Using the following `candidateBuildingParameters`:" )
    logger.debug( derConfig )

    # Decorations 
    ## Decorations -> Misc.
    df[ "infoEventNumber" ] = df[ "infoEventNumber" ]
    df[ "Electron" ] = df[ "Electron" ].apply( lambda x: [ { **i,  **{ "mass": constants.masses[ "electron" ] } } for i in x ] )
    df[ "Muon"     ] = df[ "Muon"     ].apply( lambda x: [ { **i,  **{ "mass": constants.masses[ "muon"     ] } } for i in x ] )
    df[ "Electron" ] = df[ "Electron" ].apply( lambda x: addLocalVariables( x, "z0SinTheta" ) )
    df[ "Muon"     ] = df[ "Muon"     ].apply( lambda x: addLocalVariables( x, "z0SinTheta" ) )
    ## Decorations ->  Adding Isolation Variables
    df[ "Electron" ] = df.apply( lambda x: addIsolation( x[ "Electron" ], x[ "Track" ], 0.1, "trackPtIsoCone10", relativeIsoName = "trackPtRelIsoCone10" ), axis = 1 )
    df[ "Electron" ] = df.apply( lambda x: addIsolation( x[ "Electron" ], x[ "Track" ], 0.2, "trackPtIsoCone20", relativeIsoName = "trackPtRelIsoCone20" ), axis = 1 )
    df[ "Electron" ] = df.apply( lambda x: addIsolation( x[ "Electron" ], x[ "Track" ], 0.3, "trackPtIsoCone30", relativeIsoName = "trackPtRelIsoCone30" ), axis = 1 )
    df[ "Muon"     ] = df.apply( lambda x: addIsolation( x[ "Muon"     ], x[ "Track" ], 0.1, "trackPtIsoCone10", relativeIsoName = "trackPtRelIsoCone10" ), axis = 1 )
    df[ "Muon"     ] = df.apply( lambda x: addIsolation( x[ "Muon"     ], x[ "Track" ], 0.2, "trackPtIsoCone20", relativeIsoName = "trackPtRelIsoCone20" ), axis = 1 )
    df[ "Muon"     ] = df.apply( lambda x: addIsolation( x[ "Muon"     ], x[ "Track" ], 0.3, "trackPtIsoCone30", relativeIsoName = "trackPtRelIsoCone30" ), axis = 1 )
    customConeSizeFunc30 = lambda x: min( 10 / x[ "pt" ], 0.3 )
    customConeSizeFunc20 = lambda x: min( 10 / x[ "pt" ], 0.2 )
    customConeSizeFunc10 = lambda x: min( 10 / x[ "pt" ], 0.1 )
    df[ "Electron" ] = df.apply( lambda x: addIsolation( x[ "Electron" ], x[ "Track" ], None, "trackPtIsoCustomCone10",  customConeSizeFunc10, relativeIsoName = "trackPtRelIsoCustomCone10" ), axis = 1 )
    df[ "Electron" ] = df.apply( lambda x: addIsolation( x[ "Electron" ], x[ "Track" ], None, "trackPtIsoCustomCone20",  customConeSizeFunc20, relativeIsoName = "trackPtRelIsoCustomCone20" ), axis = 1 )
    df[ "Electron" ] = df.apply( lambda x: addIsolation( x[ "Electron" ], x[ "Track" ], None, "trackPtIsoCustomCone30",  customConeSizeFunc30, relativeIsoName = "trackPtRelIsoCustomCone30" ), axis = 1 )
    df[ "Muon"     ] = df.apply( lambda x: addIsolation( x[ "Muon"     ], x[ "Track" ], None, "trackPtIsoCustomCone10",  customConeSizeFunc10, relativeIsoName = "trackPtRelIsoCustomCone10" ), axis = 1 )
    df[ "Muon"     ] = df.apply( lambda x: addIsolation( x[ "Muon"     ], x[ "Track" ], None, "trackPtIsoCustomCone20",  customConeSizeFunc20, relativeIsoName = "trackPtRelIsoCustomCone20" ), axis = 1 )
    df[ "Muon"     ] = df.apply( lambda x: addIsolation( x[ "Muon"     ], x[ "Track" ], None, "trackPtIsoCustomCone30",  customConeSizeFunc30, relativeIsoName = "trackPtRelIsoCustomCone30" ), axis = 1 )
    ## Decorations -> Adding OverlapFlags
    overlapFlaggingModule = derConfig.get( "overlapFlaggingModule", None )
    if overlapFlaggingModule is None:
        logger.warning( "No overlap flagging module provided. Skipping overlap flagging." )
    jetElectronOverlapFlaggingModule = overlapFlaggingModule.get( "jetElectronOverlap", None )
    jetMuonOverlapFlaggingModule     = overlapFlaggingModule.get( "jetMuonOverlap", None )
    if jetElectronOverlapFlaggingModule is None:
        logger.warning( "No jet-electron overlap flagging sub-module provided. Skipping jet-electron overlap flagging." )
    if jetMuonOverlapFlaggingModule is None:
        logger.warning( "No jet-muon overlap flagging sub-module provided. Skipping jet-muon overlap flagging." )
    if jetElectronOverlapFlaggingModule is not None:
        for flagName, kwargsDictForElectronJetOverlap in jetElectronOverlapFlaggingModule.items():
            kwargsDictForElectronJetOverlap[ "customDeltaRFunc" ] = eval( kwargsDictForElectronJetOverlap.get( "customDeltaRFunc", "None" ) )
            k = df.apply( lambda x: addOverlapFlags( x[ "Jet" ], x[ "Electron" ], **kwargsDictForElectronJetOverlap, overflapFlagName = flagName ), axis = 1 )
            df[ "Jet"      ] = k.apply( lambda x: x[ 0 ] )
            df[ "Electron" ] = k.apply( lambda x: x[ 1 ] )
            #addOverlapFlags( df[ "Jet" ], df[ "Electron" ], **kwargsDictForElectronJetOverlap, overflapFlagName = flagName )
    if jetMuonOverlapFlaggingModule is not None:
        for flagName, kwargsDictForMuonJetOverlap in jetMuonOverlapFlaggingModule.items():
            kwargsDictForMuonJetOverlap[ "customDeltaRFunc" ] = eval( kwargsDictForMuonJetOverlap.get( "customDeltaRFunc", "None" ) )
            kwargsDictForMuonJetOverlap[ "conditionalFunc"  ] = eval( kwargsDictForMuonJetOverlap[ "conditionalFunc" ] )
            k = df.apply( lambda x: addOverlapFlags( x[ "Jet" ], x[ "Muon" ], **kwargsDictForMuonJetOverlap, overflapFlagName = flagName ), axis = 1 )
            df[ "Jet"  ] = k.apply( lambda x: x[ 0 ] )
            df[ "Muon" ] = k.apply( lambda x: x[ 1 ] )
            #addOverlapFlags( df[ "Jet" ], df[ "Muon" ], **kwargsDictForMuonJetOverlap, flaoverflapFlagNamegName = flagName )
    ## Decorations -> Adding MinDeltaR
    globals()[ "df" ] = df
    df[ "Jet"      ] = df.apply( lambda x: addMinimumDeltaR( x[ "Jet"      ], x[ "Electron" ], { "ptThreshold" : 25 }, "minDeltaRWithElectrons" ), axis = 1 )
    df[ "Jet"      ] = df.apply( lambda x: addMinimumDeltaR( x[ "Jet"      ], x[ "Muon"     ], { "ptThreshold" : 25 }, "minDeltaRWithMuons"     ), axis = 1 )
    df[ "Electron" ] = df.apply( lambda x: addMinimumDeltaR( x[ "Electron" ], x[ "Jet"      ], { "ptThreshold" : 25 }, "minDeltaRWithJets" ), axis = 1 )
    df[ "Muon"     ] = df.apply( lambda x: addMinimumDeltaR( x[ "Muon"     ], x[ "Jet"      ], { "ptThreshold" : 25 }, "minDeltaRWithJets" ), axis = 1 )

    # Pre-Vertexing Event Selection    
    
    ## Pre-Vertexing Event Selection -> Object-Level Cuts
    
    elPtCut                       = derConfig.get( "electronPtCut"                 ,  -1  )
    muPtCut                       = derConfig.get( "muonPtCut"                     ,  -1  )
    elEtaCut                      = derConfig.get( "electronEtaCut"                , 1e8  )
    muEtaCut                      = derConfig.get( "muonEtaCut"                    , 1e8  )
    jetPtCut                      = derConfig.get( "jetPtCut"                      ,  -1  )
    jetEtaCut                     = derConfig.get( "jetEtaCut"                     , 1e8  )
    metCut                        = derConfig.get( "metCut"                        ,  -1  )
    elZ0SinThetaCut               = derConfig.get( "electronZ0SinThetaCut"         , 1e8  )
    muZ0SinThetaCut               = derConfig.get( "muonZ0SinThetaCut"             , 1e8  )
    elD0Cut                       = derConfig.get( "electronD0Cut"                 , 1e8  )
    muD0Cut                       = derConfig.get( "muonD0Cut"                     , 1e8  )
    elIsoVar                      = derConfig.get( "electronIsolationVariable"     , None )
    muIsoVar                      = derConfig.get( "muonIsolationVariable"         , None )
    elIsoCut                      = derConfig.get( "electronIsolationCut"          , 1e8  )
    muIsoCut                      = derConfig.get( "muonIsolationCut"              , 1e8  )
    electronJetOverlapRemovalFlag = derConfig.get( "electronJetOverlapRemovalFlag" , None )
    muonJetOverlapRemovalFlag     = derConfig.get( "muonJetOverlapRemovalFlag"     , None )
    requireExactlyOnePromptLepton = derConfig.get( "requireExactlyOnePromptLepton" , False )

    df[ "nElectronsOrig" ] = df[ "Electron" ].apply( lambda x: len( x ) )
    df[ "nMuonsOrig"     ] = df[ "Muon"     ].apply( lambda x: len( x ) )
    df[ "nJetsOrig"      ] = df[ "Jet"      ].apply( lambda x: len( x ) )

    df[ "Electron" ] = df[ "Electron" ].apply( lambda x: [ x for x in x if x[ "pt" ] >  elPtCut ] )
    cutflow.addCut( "ElectronPtCut", df )
    df[ "Electron" ] = df[ "Electron" ].apply( lambda x: [ x for x in x if abs( x[ "eta" ] ) <  elEtaCut ] )
    cutflow.addCut( "ElectronEtaCut", df )
    df[ "Electron" ] = df[ "Electron" ].apply( lambda x: [ x for x in x if abs( x[ "z0SinTheta" ] ) < elZ0SinThetaCut ] )
    cutflow.addCut( "ElectronZ0SinThetaCut", df )
    df[ "Electron" ] = df[ "Electron" ].apply( lambda x: [ x for x in x if abs( x[ "d0" ] ) < elD0Cut ] )
    cutflow.addCut( "ElectronD0Cut", df )
    if elIsoVar is not None:
        df[ "Electron" ] = df[ "Electron" ].apply( lambda x: [ x for x in x if x[ elIsoVar ] < elIsoCut ] )
    cutflow.addCut( "ElectronIsolationCut", df )
    df[ "Muon"     ] = df[ "Muon"     ].apply( lambda x: [ x for x in x if x[ "pt" ] >  muPtCut ] )
    cutflow.addCut( "MuonPtCut", df )
    df[ "Muon"     ] = df[ "Muon"     ].apply( lambda x: [ x for x in x if abs (x[ "eta" ] ) <  muEtaCut ] )
    cutflow.addCut( "MuonEtaCut", df )
    df[ "Muon"     ] = df[ "Muon"     ].apply( lambda x: [ x for x in x if abs( x[ "z0SinTheta" ] ) < muZ0SinThetaCut ] )
    cutflow.addCut( "MuonZ0SinThetaCut", df )
    df[ "Muon"     ] = df[ "Muon"     ].apply( lambda x: [ x for x in x if abs( x[ "d0" ] ) < muD0Cut ] )
    cutflow.addCut( "MuonD0Cut", df )
    if muIsoVar is not None:
        df[ "Muon"     ] = df[ "Muon"     ].apply( lambda x: [ x for x in x if x[ muIsoVar ] < muIsoCut ] )
    cutflow.addCut( "MuonIsolationCut", df )
    df[ "Jet"      ] = df[ "Jet"      ].apply( lambda x: [ x for x in x if x[ "pt" ] > jetPtCut ] )
    cutflow.addCut( "JetPtCut", df )
    df[ "Jet"      ] = df[ "Jet"      ].apply( lambda x: [ x for x in x if abs( x[ "eta" ] ) < jetEtaCut ] )
    cutflow.addCut( "JetEtaCut", df )

    ## Pre-Vertexing Event Selection -> Overlap Removal
    if electronJetOverlapRemovalFlag is not None:
        df[ "Electron" ] = df[ "Electron" ].apply( lambda x: [ x for x in x if not x[ electronJetOverlapRemovalFlag ] ] )
        df[ "Jet"      ] = df[ "Jet" ].apply( lambda x: [ x for x in x if not x[ electronJetOverlapRemovalFlag ] ] )
    cutflow.addCut( "ElectronJetOverlapRemoval", df )
    if muonJetOverlapRemovalFlag is not None:
        df[ "Muon"     ] = df[ "Muon" ].apply( lambda x: [ x for x in x if not x[ muonJetOverlapRemovalFlag ] ] )
        df[ "Jet"      ] = df[ "Jet" ].apply( lambda x: [ x for x in x if not x[ muonJetOverlapRemovalFlag ] ] )
    cutflow.addCut( "MuonJetOverlapRemoval", df )

    ## Pre-Vertexing Event Selection -> Event-Level Cuts

    ### Event-Level Cuts -> Leptons

    df[ "nElectrons" ] = df[ "Electron" ].apply( lambda x: len( x ) )
    df[ "nMuons"     ] = df[ "Muon"     ].apply( lambda x: len( x ) )
    
    dfExactlyOne = df[ ( ( df[ "nElectrons" ] == 1 ) & ( df[ "nMuons" ] == 0 ) ) | ( ( df[ "nElectrons" ] == 0 ) & ( df[ "nMuons" ] == 1 ) ) ]
    dfAtLeastOne = df[ ( df[ "nElectrons" ] + df[ "nMuons" ] ) >= 1 ]
    if requireExactlyOnePromptLepton:
        df = dfExactlyOne
        #cutflow.addCut( "AtLeast1PromptLepton", dfAtLeastOne, True )
        cutflow.addCut( "Exactly1PromptLepton", df )
    else:
        df = dfAtLeastOne
        #cutflow.addCut( "Exactly1PromptLepton", dfExactlyOne, True )
        cutflow.addCut( "AtLeast1PromptLepton", df )

    ### Event-Level Cuts -> Jets

    df[ "nJets" ] = df[ "Jet" ].apply( lambda x: len( x ) )
    df = df[ df[ "nJets" ] >= 4 ]
    cutflow.addCut( "AtLeast4JetsPassingCuts", df )

    ### Event-Level Cuts -> MET

    df = df[ df[ "MissingET" ].apply( lambda x: x[ "et" ] > metCut ) ]
    cutflow.addCut( "MissingETCuts", df )

    # Vertexing

    ## Vertexing -> Leptonic W Building
    mTWThreshold = derConfig.get( "mTWThresholdForLepW" , -1    )
    if requireExactlyOnePromptLepton:
        df[ "Lepton" ] = np.where( df[ "nElectrons" ] == 1, df[ "Electron" ], df[ "Muon" ] )
    else:
        df[ "Lepton" ] = df[ "Electron" ] + df[ "Muon" ]
    df[ "nLeptons" ] = df[ "Lepton" ].apply( lambda x: len( x ) )
    logger.info  ( "Building Leptonic W candidates..." )
    logger.debug ( f"\tThreshold for Transverse Mass of Leptonic W: {mTWThreshold}" )
    df[ "LepW"  ] = [ getLepWCandidates( df.iloc[ i ], mTWThreshold = mTWThreshold ) for i in tqdm( range( len( df ) ) ) ]
    df[ "nLepW" ] = df[ "LepW" ].apply( lambda x: len( x ) )
    logger.debug( f"Found {(df['nLepW']>=1).sum()} events with at least 1 LepW. Total of {df['nLepW'].sum()} LepW candidates." )
    df = df[ df[ "nLepW" ] >= 1 ]
    cutflow.addCut( "AtLeast1LepW", df[ df[ "nLepW" ] >= 1 ] )

    ## Vertexing -> Hadronic W Building
    vetoBTagged = derConfig.get( "vetoBTagForHadW"  , False )
    logger.info  ( "Building Hadronic W candidates..." )
    logger.debug ( f"\tVetoing b-Tagged Jets? {'Yes' if vetoBTagged else 'No'}!" )
    df[ "HadW"  ] = [ getHadWCandidates( df.iloc[ i ], vetoBTagged ) for i in tqdm( range( len( df ) ) ) ]
    df[ "nHadW" ] = df[ "HadW" ].apply( lambda x: len( x ) )
    logger.debug( f"Found {(df['nHadW']>=1).sum()} events with at least 1 hadW. Total of {df['nHadW'].sum()} hadW candidates." )
    df = df[ df[ "nHadW" ] >= 1 ]
    cutflow.addCut( "AtLeast1HadW", df[ df[ "nHadW" ] >= 1 ] )

    ## Vertexing -> Leptonic Top Building
    requireBTagged = derConfig.get( "requireBTagForLepTop", False ) 
    logger.info( "Building Leptonic Top candidates..." )
    df[ "LepTop"  ] = [ getLepTopCandidates( df.iloc[ i ], requireBTagged = requireBTagged ) for i in tqdm( range( len( df ) ) ) ]
    df[ "nLepTop" ] = df[ "LepTop" ].apply( lambda x: len( x ) )
    logger.debug( f"Found {(df['nLepTop']>=1).sum()} events with at least 1 LepTop. Total of {df['nLepTop'].sum()} LepTop candidates." )
    df = df[ df[ "nLepTop" ] >= 1 ]
    cutflow.addCut( "AtLeast1LepTop", df[ df[ "nLepTop" ] >= 1 ] )

    ## Vertexing -> Hadronic Top Building
    requireBTagged = derConfig.get( "requiteBTagForHadTop", False )
    logger.info( "Building Hadronic Top candidates..." )
    df[ "HadTop"  ] = [ getHadTopCandidates( df.iloc[ i ], requireBTagged = requireBTagged ) for i in tqdm( range( len( df ) ) ) ]
    df[ "nHadTop" ] = df[ "HadTop" ].apply( lambda x: len( x ) )
    logger.debug( f"Found {(df['nHadTop']>=1).sum()} events with at least 1 hadTop. Total of {df['nHadTop'].sum()} hadTop candidates." )
    df = df[ df[ "nHadTop" ] >= 1 ]
    cutflow.addCut( "AtLeast1HadTop", df[ df[ "nHadTop" ] >= 1 ] )

    ## TTbar Building
    logger.info( "Building TTbar candidates..." )
    requireLepBTagged = derConfig.get( "requireLepBTaggedForTTbar", False )
    requireHadBTagged = derConfig.get( "requireHadBTaggedForTTbar", False )
    df[ "TTbar"  ] = [ getTTbarCandidates( df.iloc[ i ], requireLepBTagged = requireLepBTagged, requireHadBTagged = requireHadBTagged, logger = logger ) for i in tqdm( range( len( df ) ) ) ]
    df[ "nTTbar" ] = df[ "TTbar" ].apply( lambda x: len( x ) )
    logger.debug( f"Found {(df['nTTbar']>=1).sum()} events with at least 1 ttbar. Total of {df['nTTbar'].sum()} ttbar candidates." )
    df = df[ df[ "nTTbar" ] >= 1 ]
    cutflow.addCut( "AtLeast1TTbar", df[ df[ "nTTbar" ] >= 1 ] )

    # Flattening...
    logger.info( "Flattening the DataFrame..." )
    dfFlat = [] #pd.DataFrame()
    for i in tqdm( range( len( df ) ) ):
        for j in range( len( df[ "TTbar" ].iloc[ i ] ) ):
            dfFlat.append( { 
                **df[ "TTbar" ].iloc[ i ][ j ],
                **{"infoEventNumber": df[ "infoEventNumber" ].iloc[ i ]},
                **{"infoSampleNumber": df[ "infoSampleNumber" ].iloc[ i ]},
                **{"nLepW": df[ "nLepW" ].iloc[ i ]},
                **{"nHadW": df[ "nHadW" ].iloc[ i ]},
                **{"nLepTop": df[ "nLepTop" ].iloc[ i ]},
                **{"nHadTop": df[ "nHadTop" ].iloc[ i ]},
                **{"nTTbar": df[ "nTTbar" ].iloc[ i ]},
                **{"nElectronsOrig": df[ "nElectronsOrig" ].iloc[ i ]},
                **{"nMuonsOrig": df[ "nMuonsOrig" ].iloc[ i ]},
                **{"nJetsOrig": df[ "nJetsOrig" ].iloc[ i ]},
                **{"MET_et": df[ "MissingET" ].iloc[ i ][ "et" ]},
                **{"MET_phi": df[ "MissingET" ].iloc[ i ][ "phi" ]} 
            })
    dfFlat = pd.DataFrame( dfFlat )
    if dsid is not None:
        dfFlat[ "infoSampleNumber" ] = int( dsid )
    logger.debug( f"Before dropping NaNs: {len(dfFlat)}" )
    dfFlat = dfFlat.dropna( axis = 1 )
    logger.debug( f"After dropping NaNs: {len(dfFlat)}" )
    return df, cutflow.getCutflow(), dfFlat
