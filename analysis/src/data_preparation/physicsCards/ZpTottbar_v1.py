### Imports ###
### ------- ###

### Imports -> Matrix/Array Manipulation ###
### ------------------------------------ ###
import numpy as np
import pandas as pd

### Imports -> Scientific Utilities ###
### ------------------------------- ###
import vector

### Imports -> Internals ###
### -------------------- ###
from src.utils import constants
from src.utils import logger as logging
from src.data_preparation import utils

### Physics Card ###
### ------------ ###

def card(tree, dsid=None, logger=None, **kwargs):
    if logger is None:
        logger = logging.get_logger(__name__)
    df = pd.DataFrame()
    if "containerConfig" not in kwargs.keys():
        logger.critical(
            "`containerConfig` is needed for `ZpTottbar_v1` physics card. Update your ntuples config!"
        )
        raise ValueError(
            "`containerConfig` is needed for `ZpTottbar_v1` physics card. Update your ntuples config!"
        )
    objectDict = kwargs["containerConfig"]
    eventNumbers = np.array(tree["Event/Event.Number"].array())
    for object in objectDict.keys():
        logger.info(f"\tProcessing {object}...")
        #if object == "MissingET":
        #    continue
        tVars = [
            f"{object}/{object}.{feature[0]}" # Relates to the naming of variables in Delphes output
            for feature in objectDict[object]["features"]
        ]
        dVars = [feature[1] for feature in objectDict[object]["features"]]
        varsDict = {tVars[i]: dVars[i] for i in range(len(tVars))}
        #_df = utils.getJaggedDataframe(tree, tVars, varsDict=varsDict)
        _df = utils.getNestedJaggedDataframe(tree, tVars, nestOwner=object, varsDict=varsDict, varType=objectDict[object]["type"])
        #import copy # debug
        #globals()[f"{object}_df"] = copy.deepcopy(_df) # debug
        #_df = utils.harmonizeJaggedDataframe(_df,
        #                                     dVars,
        #                                     nMax=objectDict[object]["nMax"])
        #_df = utils.sortJaggedDataframe(
        #    _df, varsDict[f"{object}/{objectDict[object]['sort']}"], dVars)
        #_df = utils.flattenJaggedDataframe(_df,
        #                                   dVars,
        #                                   nMax=objectDict[object]["nMax"])
        df = pd.concat([df, _df], axis=1)
    df["infoEventNumber"] = eventNumbers
    if dsid is not None:
        df["infoSampleNumber"] = np.full(len(df), int(dsid))
    return df
