### Imports ###
### ------- ###

### Imports -> Matrix/Array Manipulation ###
### ------------------------------------ ###
import numpy as np
import pandas as pd

### Imports -> Scientific Utilities ###
### ------------------------------- ###
import vector

### Imports -> Internals ###
### -------------------- ###
from src.utils import constants
from src.utils import configure
from src.utils import logger as logging
from src.data_preparation import utils


### Physics Card ###
### ------------ ###
def card(tree, dsid=None, logger=None, **kwargs):
    if logger is None:
        logger = logging.get_logger(__name__)
    df = pd.DataFrame()
    defaultContainerConfig = configure.load_config(
        configure.getPath("configs/defaultContainerConfig_ZpToMuMu.json"))
    userContainerConfig = kwargs.get("containerConfig", {})
    containerConfig = {**defaultContainerConfig, **userContainerConfig}
    for object in containerConfig.keys():
        logger.info(f"\tProcessing {object}...")
        tVars = [feature[0] for feature in containerConfig[object]["features"]]
        dVars = [feature[1] for feature in containerConfig[object]["features"]]
        varsDict = {tVars[i]: dVars[i] for i in range(len(tVars))}
        _df = utils.getJaggedDataframe(tree, tVars, varsDict=varsDict)

        _df = utils.harmonizeJaggedDataframe(
            _df, dVars, nMax=containerConfig[object]["nMax"])
        _df = utils.sortJaggedDataframe(
            _df, varsDict[containerConfig[object]["sort"]], dVars)
        _df = utils.flattenJaggedDataframe(
            _df, dVars, nMax=containerConfig[object]["nMax"])
        df = pd.concat([df, _df], axis=1)
    if dsid is not None:
        df["info_sample"] = np.full(len(df), dsid)
    globals()["df"] = df
    muon0_p4 = vector.MomentumObject4D.from_rhophietatau(
        np.array(df["muon_pt.0"]), np.array(df["muon_eta.0"]),
        np.array(df["muon_phi.0"]), constants.masses["muon"])
    muon1_p4 = vector.MomentumObject4D.from_rhophietatau(
        np.array(df["muon_pt.1"]), np.array(df["muon_eta.1"]),
        np.array(df["muon_phi.1"]), constants.masses["muon"])
    bjet_p4 = vector.MomentumObject4D.from_rhophietatau(
        np.array(df["bjet_pt"]), np.array(df["bjet_eta"]),
        np.array(df["bjet_phi"]), constants.masses["muon"])
    jet_p4 = vector.MomentumObject4D.from_rhophietatau(
        np.array(df["jet_pt"]), np.array(df["jet_eta"]),
        np.array(df["jet_phi"]), constants.masses["muon"])
    Zp_p4 = muon0_p4 + muon1_p4
    
    df["Zp_mass"] = Zp_p4.mass
    df["Zp_pt"] = Zp_p4.pt
    df["Zp_eta"] = Zp_p4.eta
    df["Zp_phi"] = Zp_p4.phi
    df["Zp_energy"] = Zp_p4.E
    df["muon_energy.0"] = muon0_p4.E
    df["muon_energy.1"] = muon1_p4.E
    df["deltaR_Zp_bjet"] = bjet_p4.deltaR(Zp_p4)
    df["deltaR_Zp_jet"] = jet_p4.deltaR(Zp_p4)
    df["deltaR_Zp_muon.0"] = muon0_p4.deltaR(Zp_p4)
    df["deltaR_Zp_muon.1"] = muon1_p4.deltaR(Zp_p4)
    df["deltaR_bjet_jet"] = bjet_p4.deltaR(jet_p4)
    df["deltaR_muon.0_bjet"] = muon0_p4.deltaR(bjet_p4)
    df["deltaR_muon.0_jet"] = muon0_p4.deltaR(jet_p4)
    df["deltaR_muon.1_bjet"] = muon1_p4.deltaR(bjet_p4)
    df["deltaR_muon.1_jet"] = muon1_p4.deltaR(jet_p4)
    df["deltaR_muon.0_muon.1"] = muon0_p4.deltaR(muon1_p4)

    return df
