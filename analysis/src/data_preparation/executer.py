from src.data_preparation import ntuplemaker
from src.data_preparation import utils
from src.utils import ioutils
from collections import OrderedDict

def process(config, logger, mode = "ntuples"):
    input_files_dict = [ (i[0], ioutils.get_files( i[1], config["input_dir"] ) ) for i in config["input_files"] ]
    dsids = [ [ str(i[0]) ] * len(i[1]) for i in input_files_dict ]
    dsids = [ item for sublist in dsids for item in sublist ]
    ntuples = [ item for sublist in [ i[1] for i in input_files_dict ] for item in sublist ]
    for idx in range(len(ntuples)):
        if dsids[ idx ] == "__dynamic__":
            if "dsidConfig" not in config.keys():
                dsids[ idx ] = None
            else:
                dsids[ idx ] = utils.get_dsid( ntuples[ idx ], config[ "dsidConfig" ] )
    if mode == "ntuples":
        ntuplemaker.NTupleMaker(config, logger).run(ntuples, dsids=dsids)
    elif mode == "derivedNTuples":
        ntuplemaker.NTupleMaker(config, logger).runDerivation(ntuples, dsids=dsids)
    else:
        raise ValueError("Mode must be one of 'ntuples' or 'derivedNTuples'.")
