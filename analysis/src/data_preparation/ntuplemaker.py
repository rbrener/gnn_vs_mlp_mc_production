### Imports ###
### ------- ###

### Imports -> I/O Utilities ###
### ------------------------ ###
import os
import uproot

### Imports -> Internals ###
### -------------------- ###
from src.utils import logger as logging
from src.data_preparation import physicsCards
from src.utils import ioutils

### Class Definition ###
### ---------------- ###


class NTupleMaker:

    def __init__(self, config, logger=None):
        self.config = config
        self.logger = logging.get_logger(
            __name__) if logger is None else logger

    def load_ntuple(self, ntuple, tree_name=None):
        tree_name = (self.config.get("tree_name", None)
                     if tree_name is None else tree_name)
        ttree = ioutils.load_ntuple(ntuple, tree_name)
        return ttree
    
    def load_feather(self, feather):
        return ioutils.load_feather(feather)

    def run(self, ntuples, dsids=None):
        for counter, ntuple in enumerate(ntuples):
            self.logger.info(
                f"Processing [{counter+1}/{len(ntuples)}]: {ntuple}...")
            for _, tree_info in self.config["trees"].items():
                events = self.load_ntuple(ntuple,
                                          tree_name=tree_info["tree_name"])
                physicsCard = getattr(physicsCards, tree_info["physics"]).card
                df = physicsCard(events,
                                 dsid=dsids[counter],
                                 logger=self.logger,
                                 **tree_info)
                os.system(f"mkdir -p {self.config.get('output_dir', '')}")
                output_file = os.path.join(
                    self.config.get("output_dir", ""),
                    os.path.basename(ntuple).replace(
                        ".root", f"_{tree_info['tree_name']}.ftr"),
                )
                df.to_feather(output_file)
            self.logger.info(f"\tSaved to {output_file}!")
    def runDerivation(self, feathers, dsids=None):
        for counter, feather in enumerate(feathers):
            self.logger.info(
                f"Processing [{counter+1}/{len(feathers)}]: {feather}...")
            events = self.load_feather(feather )
            events = events[ :10000 ]
            physicsCard = getattr(physicsCards, self.config["physics"]).card
            df, cutflow, dfFlat = physicsCard(events, dsid=dsids[counter],logger=self.logger, candidateBuildingParameters=self.config["candidateBuildingParameters"])
            os.system(f"mkdir -p {self.config.get('output_dir', '')}")
            output_file = os.path.join(
                    self.config.get("output_dir", ""),
                    os.path.basename(feather).replace(
                        ".ftr", f"_{self.config['physics']}.ftr"),
            )
            dfFlat.to_feather(output_file)
            self.logger.info(f"\tSaved to {output_file}!")
            output_file_cutflow = os.path.join(
                    self.config.get("output_dir", ""),
                    os.path.basename(feather).replace(
                        ".ftr", f"_{self.config['physics']}_cutflow.csv"),
            )
            cutflow.T.to_csv( output_file_cutflow )
            self.logger.info(f"\tSaved to {output_file_cutflow}!")
