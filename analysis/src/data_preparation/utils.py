import numpy as np
import pandas as pd
import awkward as ak
import copy
import collections
import vector
import inspect

def df_to_namedtuple(df, name="events"):
    namedClass = collections.namedtuple(name, df.columns)
    values = [ak.from_iter(df[col].tolist()) for col in df.columns]
    namedTuple = namedClass._make(values)
    return namedTuple


def getJaggedDataframe(t, vars, df=None, varsDict={}):
    if df is None:
        df = pd.DataFrame()
    for var in vars:
        _var = varsDict.get(var, var)
        df[_var] = [np.array(i, dtype=np.float64) for i in t[var].array()]
    return df

def getNestedJaggedDataframe(t, vars, nestOwner, df=None, varsDict={}, varType = "local" ):
    if df is None:
        df = pd.DataFrame()
    for var in vars:
        _var = varsDict.get(var, var)    
        df["__temp__"+_var] = [np.array(i, dtype=np.float64) for i in t[var].array()]
    #nObj = len(df["__temp__"+vars[0]])
    if varType == "local":
        df[ nestOwner ] = df.apply( lambda x: [ { k.replace("__temp__",""): x[k][idx] for k in x.keys() if k.startswith("__temp__") } for idx in range(len(x[list(x.keys())[0]])) ], axis=1 )
    elif varType == "pseudolocal":
        df[ nestOwner ] = df.apply( lambda x: { k.replace("__temp__",""): x[k][0] for k in x.keys() if k.startswith("__temp__") }, axis=1 )
    elif varType == "global":
        df[ nestOwner ] = df.apply( lambda x: { k.replace("__temp__",""): x[k] for k in x.keys() if k.startswith("__temp__") }, axis=1 ) 
    else:
        raise ValueError("varType must be one of 'local', 'pseudolocal', or 'global'.")
    df = df.drop( columns=[ k for k in df.keys() if k.startswith("__temp__") ] )
    #globals()["nestOwner"] = nestOwner # debug
    #globals()[ "df" ] = df # debug
    #exit(1) # debug
    return df

def harmonizeJaggedDataframe(df, vars, nMax=100, inplace=True):
    if not inplace:
        df = copy.deepcopy(df)
    for var in vars:
        df[var] = df[var].apply(lambda x: np.pad(
            x, (0, max(nMax - len(x), 0)), 'constant', constant_values=np.nan))
        df[var] = df[var].apply(lambda x: x[:nMax])
    return df


def sortJaggedDataframe(df, source, targets, ascending=False, inplace=True):
    if not inplace:
        df = copy.deepcopy(df)
    for target in targets:
        if ascending:
            df[target] = df.apply(
                lambda x:
                [i for i in np.array(x[target])[np.argsort(x[source])]],
                axis=1)
        else:
            df[target] = df.apply(lambda x: [
                i
                for i in np.array(x[target])[np.argsort(-np.array(x[source]))]
            ],
                                  axis=1)
    return df


def flattenJaggedDataframe(df, vars, nMax=100, inplace=True, delimiter='.'):
    if not inplace:
        df = copy.deepcopy(df)
    df = harmonizeJaggedDataframe(df, vars, nMax=nMax, inplace=True)
    _df = {}
    for var in vars:
        for i in range(nMax):
            if nMax == 1:
                _df[var] = df[var].apply(lambda x: x[0])
            else:
                _df[var + delimiter + str(i)] = df[var].apply(lambda x: x[i])
    _df = pd.DataFrame(_df)
    return _df

class cutFlower:
    def __init__( self, df, lenFuncsDict = None ):
        self.lenFuncsDict = lenFuncsDict
        self.cutflowDict = { 
            name: { "__all__": lenFunc( df ) } for name, lenFunc in self.lenFuncsDict.items() 
        }
    def addCut( self, cutName, dfCut ):
        for name, lenFunc in self.lenFuncsDict.items():
            self.cutflowDict[name].update(
                { cutName: lenFunc( dfCut ) }
            )
    def getCutflow( self ):
        return pd.DataFrame( self.cutflowDict ).T
        
def get_dsid(ntuple, dsidConfig):
    for dsidRule in dsidConfig:
        if dsidRule[1] in ntuple:
            return dsidRule[0]
    return None

class randomDebugger:
    def __init__( self, logger ):
        self.logger = logger
        self.funcDict = {}
    def addFunc( self, func, probaDebug = 0.01 ):
        self.funcDict.update( { func: probaDebug } )
    def run( self, msg = "Is saying 'no debug' a debug?" ):
        callerFunc = inspect.stack()[1][3]
        if np.random.rand() < self.funcDict.get( callerFunc, 0 ):
            self.logger.debug( f"Debugging {callerFunc}: {msg}" )
            return True
        
