#!/bin/bash

# Run as:
# ./ntupXLocalRun.sh <inputFile> <config>

### Reading the Arguments ###
### --------------------- ###

export INPUTFILE=$1
export CONFIG=$2
export BASEPATH="/storage/agrp/dvijm/gnn_vs_mlp_mc_production/analysis"

### Setup ###
### ----- ###

cd ${BASEPATH}
source setup.sh virtual
cd run 

### Running the Job ###
### --------------- ###
echo "CONFIG IS ${CONFIG}"
echo "INPUTFILE IS ${INPUTFILE}"
python3 run.py --config ${CONFIG} --action process_ntuples --batch_arguments "__dynamic__" ${INPUTFILE}
