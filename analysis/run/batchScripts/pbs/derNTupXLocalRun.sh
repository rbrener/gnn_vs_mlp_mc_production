#!/bin/bash

# Run as:
# ./derNTupXLocalRun.sh <inputFile> <config>

### Reading the Arguments ###
### --------------------- ###

export INPUTFILE=$1
export CONFIG=$2
export BASEPATH="/storage/agrp/dvijm/gnn_vs_mlp_mc_production/analysis"

### Setup ###
### ----- ###

cd ${BASEPATH}
source setup.sh virtual
cd run 

### Running the Job ###
### --------------- ###
python3 -i run.py --config ${CONFIG} --action run_derivation --batch_arguments "__dynamic__" ${INPUTFILE}
