#!/bin/bash

### Parameters Defining Locations ###
### ----------------------------- ###

export BASEPATH="/storage/agrp/dvijm/gnn_vs_mlp_mc_production/analysis"
export LOGDIR="${BASEPATH}/run/logs/"
export DATADIR="/storage/agrp/dvijm/gnn_vs_mlp_mc_production/analysis/data/feather"
export CONFIG="configs/config_zpttbar_v1.yaml"

### Parameters Defining the ML Job ###
### ------------------------------ ###

### Default Values ###
### -------------- ###

export name="GNNvsMLP"

### Submitting the Batch Jobs ###
### ------------------------- ###

declare -a INPUTFILEsLepHad=("${DATADIR}/Zprimettbar_lephad_Delphes_Smeared_RandomSeed10*.ftr" "${DATADIR}/ttbar_lephad_Delphes_Smeared_RandomSeed10*.ftr")
declare -a INPUTFILEsHadLep=("${DATADIR}/Zprimettbar_hadlep_Delphes_Smeared_RandomSeed10*.ftr" "${DATADIR}/ttbar_hadlep_Delphes_Smeared_RandomSeed10*.ftr")
declare -a INPUTFILEs=( "${INPUTFILEsLepHad[@]}" "${INPUTFILEsHadLep[@]}" )
for _INPUTFILE in "${INPUTFILEs[@]}"
do
    for INPUTFILE in $(ls ${_INPUTFILE})
    do
        echo ${INPUTFILE}
        qsub -q N -N ${name} -v BASEPATH="${BASEPATH}",CONFIG="${CONFIG}",INPUTFILE=${INPUTFILE} -o ${LOGDIR} -e ${LOGDIR} derNTupXPBSRun.sh
    done
done
