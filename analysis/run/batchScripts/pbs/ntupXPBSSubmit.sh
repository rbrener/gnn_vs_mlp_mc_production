#!/bin/bash

### Parameters Defining Locations ###
### ----------------------------- ###

export BASEPATH="/storage/agrp/dvijm/gnn_vs_mlp_mc_production/analysis"
export LOGDIR="${BASEPATH}/run/logs/"
export DATADIR="/storage/agrp/roybr/Pythia8/Delphes_Outputs/Zprimettbar"
export CONFIG="configs/config_zpttbar_v1.yaml"

### Parameters Defining the ML Job ###
### ------------------------------ ###

### Default Values ###
### -------------- ###

export name="GNNvsMLP"

### Submitting the Batch Jobs ###
### ------------------------- ###

declare -a INPUTFILEsLepHad=("${DATADIR}/Sig/Zprimettbar_lephad_Delphes_Smeared_RandomSeed*.root" "${DATADIR}/Bkg/ttbar_lephad_Delphes_Smeared_RandomSeed*.root")
declare -a INPUTFILEsHadLep=("${DATADIR}/Sig/Zprimettbar_hadlep_Delphes_Smeared_RandomSeed*.root" "${DATADIR}/Bkg/ttbar_hadlep_Delphes_Smeared_RandomSeed*.root")
#declare -a INPUTFILEsLepLep=("${DATADIR}/Sig/Zprimettbar_leplep_Delphes_Smeared_RandomSeed*.root" "${DATADIR}/Bkg/ttbar_leplep_Delphes_Smeared_RandomSeed*.root")
#declare -a INPUTFILEsHadHad=("${DATADIR}/Sig/Zprimettbar_hadhad_Delphes_Smeared_RandomSeed*.root" "${DATADIR}/Bkg/ttbar_hadhad_Delphes_Smeared_RandomSeed*.root")
declare -a INPUTFILEs=( "${INPUTFILEsLepHad[@]}" "${INPUTFILEsHadLep[@]}" "${INPUTFILEsLepLep[@]}" "${INPUTFILEsHadHad[@]}" )
for _INPUTFILE in "${INPUTFILEs[@]}"
do
    for INPUTFILE in $(ls ${_INPUTFILE})
    do
        echo ${INPUTFILE}
        qsub -q N -N ${name} -v BASEPATH="${BASEPATH}",CONFIG="${CONFIG}",INPUTFILE=${INPUTFILE} -o ${LOGDIR} -e ${LOGDIR} ntupXPBSRun.sh
    done
done
