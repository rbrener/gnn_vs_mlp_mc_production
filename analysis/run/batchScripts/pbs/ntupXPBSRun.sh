#!/bin/bash

### PBS Job Specifications ###
### ---------------------- ###

#PBS -m n
#PBS -l select=1:ncpus=1:mem=16gb -l walltime=02:00:00 -l io=2

### Setting Up the Environment ###
### -------------------------- ###

cd ${BASEPATH}
cd run
mkdir -p logs

### Running the Job ###
### --------------- ###

source ntupXLocalRun.sh ${INPUTFILE} ${CONFIG}
