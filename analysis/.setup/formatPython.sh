#!/usr/bin/env bash

formatter="yapf -i"
# take user input for the formatter if provided
# sourced or executed?
if [[ ! -z $1 ]]; then
    formatter=$1
fi
find src/. -type f | grep ".py" | grep -v ".pyc" | grep -v ".virtualenv" | xargs ${formatter}
find run/. -type f | grep ".py" | grep -v ".pyc" | grep -v ".virtualenv" | xargs ${formatter}