#!/usr/bin/env bash

dir_source=$1
dir_target=$2
for file in ${dir_source}/*; do
    file_basename=$(basename ${file})
    file_fullname=$(readlink -f ${file})
    if [[ -h "${dir_target}/${file_basename}" ]]; then
        echo "The symbolic link ${dir_target}/${file_basename} already exists."
        echo "    Relinking it..."
        echo "        Original: ${dir_target}/${file_basename} -> $(readlink -f ${dir_target}/${file_basename})"
        echo "        New: ${dir_target}/${file_basename} -> ${file_fullname}"
        rm ${dir_target}/${file_basename}
        ln -s ${file_fullname} ${dir_target}/.
    elif [[ ! -e "${dir_target}/${file_basename}" ]]; then
        echo "Creating a symbolic link ${dir_target}/${file_basename} -> ${file_fullname}"
        ln -s ${file_fullname} ${dir_target}/.
    else
        echo "The file ${dir_target}/${file_basename} exists and is not a symbolic link."
        echo "    Leaving it as is..."
    fi
done
