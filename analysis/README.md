## Table of Contents


- [Table of Contents](#table-of-contents)
- [Introduction ](#introduction-)
- [Workflow ](#workflow-)
  - [0. Setup ](#0-setup-)
    - [First-Ever Setup](#first-ever-setup)
    - [Recurrent Setup](#recurrent-setup)
  - [1. Preliminary Processing of `ntuple`s ](#1-preliminary-processing-of-ntuples-)
    - [**Steps**](#steps)
    - [**Explanation**](#explanation)
- [Repository Architecture ](#repository-architecture-)

## Introduction <a name="introduction"></a>

This sub-module of the repository contains the machinery necessary to

- Create `feather` files from `root` files. More high-level features needed for the analysis can be created during this step and added to the `feather` files.
- Do basic analysis on the `feather` files, e.g., plotting signal v/s background, correlation matrices, cutflows, etc.
- Design, train, and analyze the performance of ML models to distinguish between signal events and background events.

Every task is supposed to be steered through a config file, so the sub-module is hopefully reasonably flexible.

The `ntuple`s (provided as `.root` files) are the primary external inputs to the tasks accomplished using this sub-module of the repository. Each `ntuple` contains various simulated events of a particular high-energy process of interest.

## Workflow <a name="workflow"></a>

**Everything that is NOT described in the workflow is still WIP.**

Given these `ntuple`s, the workflow for the tasks to be accomplished using this repository is as follows:
### 0. Setup <a name="setup"></a>
#### First-Ever Setup
```
git clone https://gitlab.cern.ch/rbrener/gnn_vs_mlp_mc_production.git 
cd gnn_vs_mlp_mc_production/analysis
source setup.sh virtual
```
#### Recurrent Setup
```
cd gnn_vs_mlp_mc_production/analysis
source setup.sh virtual
```

### 1. Preliminary Processing of `ntuple`s <a name="preliminary-processing"></a>
#### **Steps**
Assuming you have finished the setup, you can now process the `ntuple`s as follows:
```
cd run
python run.py --config configs/config_v1.yaml --action process_ntuples
```
- Replace `config_v1.yaml` with the appropriate config file.
- The config file that replaces `config_v1.yaml` should have the following necessary structure for the `process_ntuples` action:
```yaml
ntuples: <some ntuple config file>
```
- The `ntuple` config file should have the following necessary structure:
```yaml
inherit: ntuples_default.yaml
trees:
   randomNameForFirstTree:
       tree_name: <name of the tree in the ntuple>
       branch_selection: <list of branches to select>
       physics: <physics process identifier>
       max_events: <maximum number of events to process>
   randomNameForSecondTree:
       tree_name: <name of the tree in the ntuple>
       branch_selection: <list of branches to select>
       physics: <physics process identifier>
       max_events: <maximum number of events to process>
input_files:
  - [ DSID1, path/to/ntuple1.root ]
  - [ DSID2, path/to/ntuple2.root ]
```
    - The `branch_selection` key should be set to `eval:None` if you want to select all branches.
    - The `physics` key should correspond to some `if` condition implemented in `src/data_preparation/ntuplemaker.py` to select the appropriate reconstruction of variables relevant to the analysis of interest.
        - In other words, `src/data_preparation/ntuplemaker.py` is the ntuplemaker where you can implement whatever higher-level features you want to extract from the raw `ntuple` information.
    - DSIDs are the unique identifiers for the `ntuple`s. They don't need to correspond to any upstream information, but they will be written out to the `feather` files -- so they matter for downstream analysis.

#### **Explanation**
- We first simplify the `ntuple`s and convert them to a much more efficiently readable format called `feather` files.
- There are various reasons for wanting to do this:
    - The `ntuple`s often contain more information than we need for the ML purposes.
    - There might as well be some derived information that we want to extract via processing the raw information available in `ntuple`s.
    - Of course, such information can be added to the `ntuple`s themselves, however, it's computationally more expensive to reproduce `ntuple`s than to reprocess the produced `ntuple`s.
    - The structure of the data in `ntuple`s is often "non-linear", i.e., it cannot be represented as a matrix but would rather need to be represented as [jagged arrays](https://en.wikipedia.org/wiki/Jagged_array).
        - These jagged arrays are not ideal to work with in `python`, especially as they grow larger.

## Repository Architecture <a name="repo-architecture"></a>

```
.
├── README.md
├── data
│   ├── feather
│   └── ntuples
├── requirements.txt
├── run
│   ├── configs
│   │   ├── config_v1.yaml
│   │   ├── gnn_v1.yaml
│   │   ├── graphs_v1.yaml
│   │   ├── ntuples_default.yaml
│   │   ├── ntuples_v1.yaml
│   │   ├── libs
│   │   │   └── graphs
│   │   │       ├── ZPrime_v1_2l0j.yaml
│   │   │       ├── ZPrime_v1_2l1j.yaml
│   │   │       └── ZPrime_v1_2l2j.yaml
│   └── run.py
├── setup.sh
├── src
│   ├── __init__.py
│   ├── analysis
│   ├── data_preparation
│   │   ├── __init__.py
│   │   ├── create_arrays.py
│   │   ├── create_graphs.py
│   │   ├── create_trees.py
│   │   ├── dataloader.py
│   │   ├── executer.py
│   │   └── ntuplemaker.py
│   ├── ml_core
│   │   ├── executer.py
│   │   ├── gnn_executer.py
│   │   ├── gnn_models.py
│   │   ├── mlp_models.py
│   │   └── modelBuilder.py
│   └── utils
│       ├── __init__.py
│       ├── configure.py
│       ├── constants.py
│       ├── dfm.py
│       ├── dictricks.py
│       ├── ioutils.py
│       ├── logger.py
│       └── system.py


```
