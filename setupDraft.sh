#!/usr/bin/bash 

### Git Config ###
### ---------- ###
git config --unset credential.helper
export GIT_TERMINAL_PROMPT=1
unset SSH_ASKPASS

### Doing the Git Update for SubModule ###
### ---------------------------------- ###
[ -f .setupDraft.sh ] && rm .setupDraft.sh
touch .setupDraft.sh
echo '#!/usr/bin/expect -f' >> .setupDraft.sh 
echo '### Git Submodule SetUp ###' >> .setupDraft.sh 
echo '### -------------------- ###' >> .setupDraft.sh 
echo 'set timeout -1' >> .setupDraft.sh 
echo '# Password' >> .setupDraft.sh 
echo 'set password "olp_6Ax5x2vVNClcYdQrWVsOntrP2YP26R42qORC"' >> .setupDraft.sh 
echo '# Start the Submodule Update' >> .setupDraft.sh 
echo 'spawn git submodule update --init --remote' >> .setupDraft.sh 
echo '# Expect the Password Prompt and Send the Password' >> .setupDraft.sh 
echo 'expect {' >> .setupDraft.sh 
echo '    -re "(P|p)assword.*:" {' >> .setupDraft.sh 
echo '        send "$password\r"' >> .setupDraft.sh 
echo '    }' >> .setupDraft.sh 
echo '    eof {' >> .setupDraft.sh 
echo '        exit' >> .setupDraft.sh 
echo '    }' >> .setupDraft.sh 
echo '}' >> .setupDraft.sh 
echo 'interact' >> .setupDraft.sh 
chmod +x .setupDraft.sh
./.setupDraft.sh  
rm .setupDraft.sh

### Setting the Branch-Tracking ###
### --------------------------- ###
cd docs
git switch master
git pull
cd ..
