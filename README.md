# gnn_vs_mlp_MC_production

Link to CodiMD [page](https://codimd.web.cern.ch/Ki-0TF7hS_6jPc3_pcI6IA#)

### General

1. This repo is NOT a standalone operating framework
2. It is used to save code from different sources used to produce MC ntuples for the project
3. Key: Pythia8 `.cc` files, Delphes `.sh` scripts *etc.*

### Pythia8

Job Options (JOs) produce either background or signal events, namely

1. `main_ttbar_generator_*.cc` generate ttbar background, output a `.hepmc` file; `*_dilep*`, `*_allhad*`, `*_lephad*`, `*_hadlep*`, used to restrict W decays in ttbar events (leptonic, hadronic, semi-leptonic)
2. `main_Zprimettbar_generator_.cc` generate Z'->ttbar signals (mZp=1TeV; large NP couplings to ttbar), output a `.hepmc` file

### Delphes

1. Shell script, `run_delphes_convert_pythia_lhe_to_root_ATLAS_detector.sh`

### Worflow

```mermaid
graph TD;
  Pythia8-->HepMC3file;
  HepMC3file-->Delphes;
  Delphes-->ROOTfile;
```

# Separate Pythia8 and local repo

1. To avoid cloning this repo inside a local Pythia8 work area, use symlink script in this repo, `interface_with_local_pythia.sh`.
2. This script symlinks all files in `mc_generation` dir of this repo to local Pythia8 work area (`/examples`).
3. To control mc generation, edit desired scripts under `mc_generation` or in local Pythia8 work area (`/examples`).
4. Make sure to edit path to your local Pythia8 work area in `interface_with_local_pythia.sh`.

```mermaid
flowchart TD
  local_repo & local_Pythia8
  mc_generation & examples
  local_repo-->mc_generation
  local_Pythia8-->examples  
   newLines["process.cc
   process_Dct.cc
   process.h
   process_LinkDef.h"]
  newLinesSym["process.cc
   process_Dct.cc
   process.h
   process_LinkDef.h"]
   mc_generation-->newLines
   examples-->newLinesSym      
```


# MC generation

| Sig./Bkg.      | Process/          | NEvents     | Delphes ready|
| -------------- | ----------------- | ----------- | -------------|
| **Sig. 1 TeV** | Zp->ttbar->lephad | 100*10k=1M | - []       |
| **Sig. 1 TeV** | Zp->ttbar->hadlep | 100*10k=1M | - []       |

| Sig./Bkg.      | Process/          | NEvents     | Delphes ready|
| -------------- | ----------------- | ----------- | -------------|
| **Sig. 1.25 TeV** | Zp->ttbar->lephad | 100*10k=1M | - []    |
| **Sig. 1.25 TeV** | Zp->ttbar->hadlep | 100*10k=1M | - []    |

| Sig./Bkg.      | Process/          | NEvents     | Delphes ready |
| -------------- | ----------------- | ----------- | --------------|
| **Sig. 1.5 TeV** | Zp->ttbar->lephad | 100*10k=1M | - []      |
| **Sig. 1.5 TeV** | Zp->ttbar->hadlep | 100*10k=1M | - []      |


| Sig/Bkg.       | Process/      | NEvents     | Delphes ready |
| -------------- | ------------- | ----------- | ------------- |
| **Bkg.** | ttbar->lephad | 1000*10k=10M | - [x]              |
| **Bkg.** | ttbar->hadlep | 1000*10k=10M | - [x]              |

To monitor production, see [sheet](https://docs.google.com/spreadsheets/d/1Q3BSxLnM9fQNE5VQrs1cIkwm7bVlgerCgjt6HW3r_jc/edit?gid=0#gid=0)

# Paper draft
(in main dir)

**If first time:**
1. `source setupDraft.sh`; ~~provide `olp_6Ax5x2vVNClcYdQrWVsOntrP2YP26R42qORC` as password when prompted~~ password is no longer needed to be typed out, you will be automatically authenticated.

**Else:**

2. Inside `docs`, treat as normal `git` repo.
	a. `cd docs && git pull` whenever you wish to pull remote changes (from Overleaf)
	b. `cd docs` ---make your changes--- `git add <files> && git commit -m "message" && git push`
3. Thereby, `docs` is synched both w/ our (this) `git` repo and Overleaf so you may either [1] edit on your computer and push changes to our repo and Overleaf or [2] edit on Overleaf, which would update the submodule's remote automatically.
4. To ensure updates to submodule are up-to-date locally, `cd docs && git pull', check consistency w/ Overleaf to confirm.
